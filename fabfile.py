# -*- coding: utf-8 -*-
from fabric.api import local


# def freeze_requirements():
# local('pip freeze > requirements.txt')


def install_requirements():
    local('pip install -r requirements.txt')


    # def format_code(python_file=''):
    #     local('autopep8 --in-place %s' % python_file)
    #
    #
    # def crear_red_caba():
    #     local('python -m ruteo.red_creator data/grafos/caba_grafo.edgelist '
    #           'data/restricciones/caba_restricciones.csv '
    #           'data/redes/caba_red.edgelist')
    #
    #
    # def crear_red_caba_distancia():
    #     local('python -m ruteo.red_creator data/grafos/caba_grafo_distancia.edgelist '
    #           'data/restricciones/caba_restricciones.csv '
    #           'data/redes/caba_red_distancia.edgelist')
    #
    #
    # def crear_red_bsas_osm_distancia():
    #     local('python -m ruteo.red_creator data/grafos/bsas_osm_grafo_distancia.edgelist '
    #           'data/restricciones/bsas_osm_restricciones.csv '
    #           'data/redes/bsas_osm_red_distancia.edgelist')
    #
    #
    # def crear_red_caba_pie_distancia():
    #     local('python -m ruteo.red_creator data/grafos/caba_grafo_pie_distancia.edgelist '
    #           'data/restricciones/caba_pie_restricciones.csv '
    #           'data/redes/caba_red_pie_distancia.edgelist')
    #
    #
    # def crear_red_bsas():
    #     local('python -m ruteo.red_creator data/grafos/bsas_grafo.edgelist '
    #           'data/restricciones/bsas_restricciones.csv '
    #           'data/redes/bsas_red.edgelist')