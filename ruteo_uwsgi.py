# -*- coding: utf-8 -*-
from flask import Flask, Response, json, request
import urllib2
import urllib
import ruteo

#recorridos_server_auto = ruteo.RecorridosServerBuilder.get_recorrido_server_auto()
#recorridos_server_auto_distancia = ruteo.RecorridosServerBuilder.get_recorrido_server_auto_distancia()
recorridos_server_pie = ruteo.RecorridosServerBuilder.get_recorrido_server_pie()
#recorridos_server_auto_bsas = ruteo.RecorridosServerBuilder.get_recorrido_server_auto_bsas()
#recorridos_server_bsas_osm_auto_distancia = ruteo.RecorridosServerBuilder.get_recorrido_server_bsas_osm_auto_distancia()

app = Flask(__name__)

from functools import wraps
import time


def timethis(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()

        print(func.__name__, end - start)

        return result

    return wrapper


@app.route('/')
def server():
    return 'Servidor de recorridos'


@app.route('/buscar_recorrido/auto/')
def buscar_recorrido_auto():
    origen = request.args.get('origen', None)
    if origen is not None:
        origen = origen.split(',')
        origen_x = float(origen[0])
        origen_y = float(origen[1])

    destino = request.args.get('destino', None)
    if destino is not None:
        destino = destino.split(',')
        destino_x = float(destino[0])
        destino_y = float(destino[1])

    origen_calle = request.args.get('origen_calle', None)
    if origen_calle is not None:
        try:
            origen_calle = int(origen_calle)
        except ValueError:
            origen_calle = str(origen_calle)

    origen_altura = request.args.get('origen_altura', None)
    if origen_altura is not None:
        origen_altura = int(origen_altura)

    origen_cruce = request.args.get('origen_cruce', None)
    if origen_cruce is not None:
        try:
            origen_cruce = int(origen_calle)
        except ValueError:
            origen_cruce = str(origen_cruce)

    destino_calle = request.args.get('destino_calle', None)
    if destino_calle is not None:
        try:
            destino_calle = int(destino_calle)
        except ValueError:
            destino_calle = str(destino_calle)

    destino_altura = request.args.get('destino_altura', None)
    if destino_altura is not None:
        destino_altura = int(destino_altura)

    destino_cruce = request.args.get('destino_cruce', None)
    if destino_cruce is not None:
        try:
            destino_cruce = int(destino_cruce)
        except ValueError:
            destino_cruce = str(destino_cruce)

    distancia = bool(int(request.args.get('distancia', 0)))
    autopistas = bool(int(request.args.get('autopistas', 0)))
    cortes = bool(int(request.args.get('cortes', 0)))

    srid_from = int(request.args.get('srid_from', ruteo.SRID_FROM))
    srid_to = int(request.args.get('srid_to', ruteo.SRID_TO))

    BASE_PATH_GEOCODING = 'http://ws.usig.buenosaires.gob.ar/geocoder/2.2/geocoding/'
    BASE_PATH_REVERSE_GEOCODING = 'http://ws.usig.buenosaires.gob.ar/geocoder/2.2/reversegeocoding/'

    if origen is None:
        if not origen_altura is None:
            params = {"cod_calle": origen_calle, "altura": origen_altura}
            data = json.loads(urllib2.urlopen(BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            origen_x = float(data['x'])
            origen_y = float(data['y'])
        else:
            params = {"cod_calle1": origen_calle, "cod_calle2": origen_cruce}
            data = json.loads(urllib2.urlopen(BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            origen_x = float(data['x'])
            origen_y = float(data['y'])
    elif origen_calle is None:
        params = {"x": origen_x, "y": origen_y}
        data = json.loads(
            urllib2.urlopen(BASE_PATH_REVERSE_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
        origen_data = data['puerta'].split(' ')
        origen_altura = int(origen_data.pop())
        origen_calle = str(' '.join(origen_data))
        origen_x = float(data['puerta_x'])
        origen_y = float(data['puerta_y'])

    if destino is None:
        if not destino_altura is None:
            params = {"cod_calle": destino_calle, "altura": destino_altura}
            data = json.loads(urllib2.urlopen(BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            destino_x = float(data['x'])
            destino_y = float(data['y'])
        else:
            params = {"cod_calle1": destino_calle, "cod_calle2": destino_cruce}
            data = json.loads(urllib2.urlopen(BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            destino_x = float(data['x'])
            destino_y = float(data['y'])

    elif destino_calle is None:
        params = {"x": destino_x, "y": destino_y}
        data = json.loads(
            urllib2.urlopen(BASE_PATH_REVERSE_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
        destino_data = data['puerta'].split(' ')
        destino_altura = int(destino_data.pop())
        destino_calle = str(' '.join(destino_data))
        destino_x = float(data['puerta_x'])
        destino_y = float(data['puerta_y'])

    parameters = dict(
        origen_calle=origen_calle,
        origen_altura=origen_altura,
        origen_cruce=origen_cruce,
        destino_calle=destino_calle,
        destino_altura=destino_altura,
        destino_cruce=destino_cruce,
        origen_x=origen_x,
        origen_y=origen_y,
        destino_x=destino_x,
        destino_y=destino_y,
        srid_from=srid_from,
        srid_to=srid_to
    )

    if not distancia:
        recorrido = recorridos_server_auto.get_recorrido(**parameters)

    else:
        recorrido = recorridos_server_auto_distancia.get_recorrido(**parameters)

    return Response(json.dumps(recorrido), mimetype='application/json')


@app.route('/buscar_recorrido/auto/bsas/')
def buscar_recorrido_auto_bsas():
    origen = request.args.get('origen', None)

    destino = request.args.get('destino', None)

    parameters = dict(
        origen=origen,
        destino=destino,
    )

    recorrido = recorridos_server_auto_bsas.get_recorrido_auto_bsas(**parameters)

    return Response(json.dumps(recorrido), mimetype='application/json')

@app.route('/buscar_recorrido_cruce/bicicleta/')
def buscar_recorrido_bicileta():
    pass


@app.route('/buscar_recorrido/pie/')
@timethis
def buscar_recorrido_pie():
    origen = request.args.get('origen', None)
    if origen is not None:
        origen = origen.split(',')
        origen_x = float(origen[0])
        origen_y = float(origen[1])

    destino = request.args.get('destino', None)
    if destino is not None:
        destino = destino.split(',')
        destino_x = float(destino[0])
        destino_y = float(destino[1])

    origen_calle = request.args.get('origen_calle', None)
    if origen_calle is not None:
        try:
            origen_calle = int(origen_calle)
        except ValueError:
            origen_calle = str(origen_calle)

    origen_altura = request.args.get('origen_altura', None)
    if origen_altura is not None:
        origen_altura = int(origen_altura)

    origen_cruce = request.args.get('origen_cruce', None)
    if origen_cruce is not None:
        try:
            origen_cruce = int(origen_calle)
        except ValueError:
            origen_cruce = str(origen_cruce)

    destino_calle = request.args.get('destino_calle', None)
    if destino_calle is not None:
        try:
            destino_calle = int(destino_calle)
        except ValueError:
            destino_calle = str(destino_calle)

    destino_altura = request.args.get('destino_altura', None)
    if destino_altura is not None:
        destino_altura = int(destino_altura)

    destino_cruce = request.args.get('destino_cruce', None)
    if destino_cruce is not None:
        try:
            destino_cruce = int(destino_cruce)
        except ValueError:
            destino_cruce = str(destino_cruce)

    srid_from = int(request.args.get('srid_from', ruteo.SRID_FROM))
    srid_to = int(request.args.get('srid_to', ruteo.SRID_TO))

    BASE_PATH_GEOCODING = 'http://ws.usig.buenosaires.gob.ar/geocoder/2.2/geocoding/'
    BASE_PATH_REVERSE_GEOCODING = 'http://ws.usig.buenosaires.gob.ar/geocoder/2.2/reversegeocoding/'

    if origen is None:
        if not origen_altura is None:
            params = {"cod_calle": origen_calle, "altura": origen_altura}
            data = json.loads(urllib2.urlopen(BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            origen_x = float(data['x'])
            origen_y = float(data['y'])
        else:
            params = {"cod_calle1": origen_calle, "cod_calle2": origen_cruce}
            data = json.loads(urllib2.urlopen(BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            origen_x = float(data['x'])
            origen_y = float(data['y'])
    elif origen_calle is None:
        params = {"x": origen_x, "y": origen_y}
        data = json.loads(
            urllib2.urlopen(BASE_PATH_REVERSE_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
        origen_data = data['puerta'].split(' ')
        origen_altura = int(origen_data.pop())
        origen_calle = str(' '.join(origen_data))
        origen_x = float(data['puerta_x'])
        origen_y = float(data['puerta_y'])

    if destino is None:
        if not destino_altura is None:
            params = {"cod_calle": destino_calle, "altura": destino_altura}
            data = json.loads(urllib2.urlopen(BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            destino_x = float(data['x'])
            destino_y = float(data['y'])
        else:
            params = {"cod_calle1": destino_calle, "cod_calle2": destino_cruce}
            data = json.loads(urllib2.urlopen(BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            destino_x = float(data['x'])
            destino_y = float(data['y'])

    elif destino_calle is None:
        params = {"x": destino_x, "y": destino_y}
        data = json.loads(
            urllib2.urlopen(BASE_PATH_REVERSE_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
        destino_data = data['puerta'].split(' ')
        destino_altura = int(destino_data.pop())
        destino_calle = str(' '.join(destino_data))
        destino_x = float(data['puerta_x'])
        destino_y = float(data['puerta_y'])

    parameters = dict(
        origen_calle=origen_calle,
        origen_altura=origen_altura,
        origen_cruce=origen_cruce,
        destino_calle=destino_calle,
        destino_altura=destino_altura,
        destino_cruce=destino_cruce,
        origen_x=origen_x,
        origen_y=origen_y,
        destino_x=destino_x,
        destino_y=destino_y,
        srid_from=srid_from,
        srid_to=srid_to
    )

    recorrido = recorridos_server_pie.get_recorrido_pie(**parameters)

    return Response(json.dumps(recorrido), mimetype='application/json')


@app.route('/buscar_recorrido/pie/lite/')
@timethis
def buscar_recorrido_pie_lite():
    origen = request.args.get('origen', None)
    if origen is not None:
        origen = origen.split(',')
        origen_x = float(origen[0])
        origen_y = float(origen[1])

    destino = request.args.get('destino', None)
    if destino is not None:
        destino = destino.split(',')
        destino_x = float(destino[0])
        destino_y = float(destino[1])

    srid_from = int(request.args.get('srid_from', ruteo.SRID_FROM))
    srid_to = int(request.args.get('srid_to', ruteo.SRID_TO))

    parameters = dict(
        origen_x=origen_x,
        origen_y=origen_y,
        destino_x=destino_x,
        destino_y=destino_y,
        srid_from=srid_from,
        srid_to=srid_to
    )

    recorrido = recorridos_server_pie.get_recorrido_pie_lite(**parameters)

    return Response(json.dumps(recorrido), mimetype='application/json')


@app.route('/buscar_recorrido_bsas_osm/auto/')
def buscar_recorrido_bsas_osm_auto():
    origen = request.args.get('origen', None)

    destino = request.args.get('destino', None)

    srid_from = int(request.args.get('srid_from', ruteo.SRID_FROM))
    srid_to = int(request.args.get('srid_to', ruteo.SRID_TO))

    parameters = dict(
        origen=origen,
        destino=destino,
    )

    recorrido = recorridos_server_bsas_osm_auto_distancia.get_recorrido_auto_bsas(**parameters)

    return Response(json.dumps(recorrido), mimetype='application/json')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)

    #app.run(debug=True)

