# -*- coding: utf-8 -*-
import csv

# "id";"source";"target";"nomoficial";"nombre";"codigo";"tipo_c";"alt_parini";"alt_parfin";"alt_imparini";"alt_imparfin";
# "vereda_par";"puente";"tunel";"cruce_ffcc";"tipo_ffcc";"cod_sent_auto";"sentido_auto";"tto_vial";"tto_bicicleta";"tto_peatonal"
#
class DatosLoader(object):
    def __init__(self):
        pass

    def load(self, file_in):
        datos = csv.reader(open(file_in), delimiter=';', quotechar='"')

        result = dict()
        for dato in datos:
            #result[(int(dato[0]), int(dato[1]))] = {

            try:
                codigo_de_calle = int(dato[5])
            except ValueError:
                codigo_de_calle = None

            try:
                altura_par_inicial = int(dato[7])
            except ValueError:
                altura_par_inicial = None

            try:
                altura_par_final = int(dato[8])
            except ValueError:
                altura_par_final = None

            try:
                altura_impar_inicial = int(dato[9])
            except ValueError:
                altura_impar_inicial = None

            try:
                altura_impar_final = int(dato[9])
            except ValueError:
                altura_impar_final = None

            try:
                sentido = int(dato[17])
            except ValueError:
                sentido = None

            result[int(dato[0])] = {
                #'tramo': int(dato[0]),
                'nodo_origen': int(dato[1]),
                'nodo_destino': int(dato[2]),
                'nombre_oficial': dato[3],
                'nombre': dato[4],
                'codigo_de_calle': codigo_de_calle,
                'tipo_de_calle': dato[6],
                'altura_par_inicial': altura_par_inicial,
                'altura_par_final': altura_par_final,
                'altura_impar_inicial': altura_impar_inicial,
                'altura_impar_final': altura_impar_final,
                'vereda': dato[11],
                'es_puente': dato[12],
                'es_tunel': dato[13],
                'es_cruce_ferroviario': dato[14],
                'tipo_de_cruce_ferroviario': dato[15],
                'codigo_sentido': int(dato[16]),
                'sentido': sentido,
                'vial': bool(dato[18]),
                'bicicleta': bool(dato[19]),
                'peatonal': bool(dato[20]),
            }

            #print result[(dato[1], dato[2])]
        return result

