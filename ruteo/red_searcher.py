# -*- coding: utf-8 -*-
import networkx as nx

import ruteo


class RedSearcher(object):
    def __init__(self, grafo, red):
        self.grafo = grafo
        self.red = red

        self.vertice_converter = ruteo.VerticeConverter()

    def buscar_recorrido_cruce(self, origen, destino):
        origen = str(origen)
        destino = str(destino)

        self._agregar_vertice_origen(self.grafo, self.red, origen)
        self._agregar_vertice_destino(self.grafo, self.red, destino)

        vertices = nx.dijkstra_path(self.red, origen, destino)

        self.red.remove_node(origen)
        self.red.remove_node(destino)

        return self.vertice_converter.contraer_vertices(vertices)
        # return map(int, self.vertice_converter.contraer_vertices(vertices))

    def buscar_recorrido(self, origen, destino):
        origen = str(origen)
        destino = str(destino)

        vertices = nx.dijkstra_path(self.red, origen, destino)

        return self.vertice_converter.contraer_vertices(vertices)

    def _agregar_vertice_origen(self, grafo, red, origen):
        for arista_out in grafo.out_edges(origen):
            vertice_out = self.vertice_converter.crear_vertice_out(arista_out)
            red.add_edge(origen, vertice_out, weight=0)
        return red

    def _agregar_vertice_destino(self, grafo, red, destino):
        for arista_in in grafo.in_edges(destino):
            vertice_in = self.vertice_converter.crear_vertice_in(arista_in)
            red.add_edge(vertice_in, destino, weight=0)
        return red

