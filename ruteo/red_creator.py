# -*- coding: utf-8 -*-
import ruteo


class RedCreator(object):
    def __init__(self):
        vertice_converter = ruteo.VerticeConverter()

        self.grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())
        self.grafo_expander = ruteo.GrafoExpander(ruteo.VerticeExpander(vertice_converter))

        self.restricciones_loader = ruteo.RestriccionesLoader()
        self.grafo_restrictor = ruteo.GrafoRestrictor(vertice_converter)

    def create(self, file_in, file_restricciones, file_out):
        grafo = self.grafo_io.load(file_in)
        restricciones = self.restricciones_loader.load(file_restricciones)

        self.get_red(grafo, restricciones)

        self.grafo_io.save(grafo, file_out)
        return grafo

    def get_red(self, grafo, restricciones):
        self.grafo_expander.expandir_grafo(grafo)
        self.grafo_restrictor.aplicar_restricciones(grafo, restricciones)
        return grafo


if __name__ == "__main__":
    import sys

    red_creator = RedCreator()

    if len(sys.argv) == 4:
        red_creator.create(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print "Faltan parámetros"