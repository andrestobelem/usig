# -*- coding: utf-8 -*-
from flask import Flask, g
import psycopg2

app = Flask(__name__)
app.config.from_object('ruteo.default_settings')

import ruteo.views


def connect_db():
    return psycopg2.connect(dbname='recorridos_prueba_osm', host='10.10.4.48', port='5432', user='usig',
                            password='usig')


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


def get_db():
    if not hasattr(g, 'db'):
        g.db = connect_db()
    return g.db


SRID_FROM = 4326
SRID_TO = 4326
SRID_IN = 4326
SRID_GEOCODER = 97433

DIR_GRAFOS = 'data/grafos/'
DIR_REDES = 'data/redes/'
DIR_RESTRICCIONES = 'data/restricciones/'
DIR_GEOMETRIAS = 'data/geometrias/'
DIR_DATOS = 'data/datos/'

TIEMPO_FACTOR = 1000

TOLERANCIA_EN_METROS = 10
TOLERANCIA_EN_GRADOS = 15

BASE_PATH_GEOCODING = 'http://ws.usig.buenosaires.gob.ar/geocoder/2.2/geocoding/'
BASE_PATH_REVERSE_GEOCODING = 'http://ws.usig.buenosaires.gob.ar/geocoder/2.2/reversegeocoding/'

from ruteo.coordenadas_loader import CoordenadasLoader
from ruteo.coordenadas_repository import CoordenadasRepository
from ruteo.geometrias_loader import GeometriasLoader
from ruteo.geometrias_repository import GeometriasRepository
from ruteo.datos_loader import DatosLoader
from ruteo.datos_repository import DatosRepository
from ruteo.recorridos_server import RecorridosServer
from ruteo.recorridos_server_builder import RecorridosServerBuilder
from ruteo.geometrias_decorator import GeometriasDecorator
from ruteo.datos_decorator import DatosDecorator
from ruteo.geojson_decorator import GeoJSONDecorator
from ruteo.segmentos_decorator import SegmentosDecorator
from ruteo.tiempos_decorator import TiemposDecorator
from ruteo.srid_decorator import SridDecorator


recorridos_server = ruteo.RecorridosServerBuilder.get_recorrido_server()
