# -*- coding: utf-8 -*-
import math

import ruteo


class SegmentosDecorator(object):
    def __init__(self, geometrias_repository, datos_repository):
        self._geometrias_repository = geometrias_repository
        self._datos_repository = datos_repository

        self._tolerancia_en_metros = ruteo.TOLERANCIA_EN_METROS
        self._tolerancia_en_grados = ruteo.TOLERANCIA_EN_GRADOS

    def _angle_wrt_x(self, A, B):
        ax, ay = A
        bx, by = B
        return math.atan2(by - ay, bx - ax)

    def _rad2deg(self, radians):
        pi = math.pi
        degrees = 180 * radians / pi

        if degrees <= -180:
            degrees += 360

        if degrees >= 180:
            degrees -= 360

        return degrees

    def decorate(self, recorrido):

        n = 0
        i = 0
        result = dict()

        it = iter(sorted(recorrido.iteritems(), key=lambda x: x[1]['orden']))
        nit = iter(sorted(recorrido.iteritems(), key=lambda x: x[1]['orden']))
        next(nit)
        cruces = None
        for k, v in it:
            try:
                nk, nv = next(nit)
            except StopIteration:
                nv = {'datos': {'nombre_oficial': None}}

            datos = v['datos']
            geometria = v['geometria']
            tiempo = 0
            longitud = 0

            if 'tiempo' in v:
                tiempo = v['tiempo']

            if 'longitud' in v:
                longitud = v['longitud']

            if n == 0:
                nodo_origen = v['datos']['nodo_origen']
                nodo_destino = v['datos']['nodo_destino']

                if not (nodo_origen == nv['datos']['nodo_origen'] or nodo_origen == nv['datos']['nodo_destino']):
                    cruces = self._datos_repository.get_datos_por_nodo_origen(nodo_origen, k[1])

                elif not (nodo_destino == nv['datos']['nodo_origen'] or nodo_origen == nv['datos']['nodo_destino']):
                    cruces = self._datos_repository.get_datos_por_nodo_destino(nodo_destino, k[1])

                segmento = Segmento(geometria, datos, tiempo, longitud, self._geometrias_repository)

            elif segmento.get_dato('nombre_oficial') == datos['nombre_oficial']:
                segmento.concatenar_linea(geometria)
                segmento.update_datos(datos)
                segmento.add_tiempo(tiempo)
                segmento.add_longitud(longitud)

            elif geometria.Length() < self._tolerancia_en_metros \
                    and datos['altura_par_inicial'] == 0 \
                    and datos['altura_impar_inicial'] == 0 \
                    and segmento.get_dato('nombre_oficial') == nv['datos']['nombre_oficial']:

                segmento.concatenar_linea(geometria)
                segmento.add_tiempo(tiempo)
                segmento.add_longitud(longitud)

            else:
                p0 = segmento.get_geometria().GetPoint_2D(segmento.get_geometria().GetPointCount() - 2)
                p1 = segmento.get_geometria().GetPoint_2D(segmento.get_geometria().GetPointCount() - 1)
                p2 = geometria.GetPoint_2D(1)

                angulo = self._rad2deg(self._angle_wrt_x(p0, p1) - self._angle_wrt_x(p1, p2))

                if abs(round(angulo)) >= self._tolerancia_en_grados and angulo < 0:
                    giro = 'izquierda'
                elif abs(round(angulo)) >= self._tolerancia_en_grados and angulo > 0:
                    giro = 'derecha'
                else:
                    giro = 'ninguno'

                result.update({
                    i: {'orden': i, 'geometria': segmento.get_geometria(),
                        'longitud': segmento.get_longitud(),
                        'tiempo': segmento.get_tiempo(),
                        'angulo': angulo,
                        'giro': giro,
                        'cruces': cruces,
                        'datos': segmento.get_datos()}})
                print result
                segmento = Segmento(geometria, datos, tiempo, longitud, self._geometrias_repository)

                i += 1

            result.update({
                i: {'orden': i, 'geometria': segmento.get_geometria(),
                    'longitud': longitud,
                    'tiempo': tiempo,
                    'datos': segmento.get_datos()}})
            n += 1

        return result


class Segmento(object):
    def __init__(self, geometria, datos, tiempo, longitud, geometrias_repository):
        self._geometrias_repository = geometrias_repository

        self._geometria = geometria
        self._keys = ('nombre_oficial',
                      'nombre',
                      'altura_par_inicial',
                      'altura_impar_inicial',
                      'altura_par_final',
                      'altura_impar_final')

        self._dictfilt = lambda x, y: dict([(i, x[i]) for i in x if i in y])

        self._datos = self._dictfilt(datos, self._keys)
        self._tiempo = tiempo
        self._longitud = longitud


    def update_datos(self, datos):
        self._datos['nombre_oficial'] = datos['nombre_oficial']
        self._datos['nombre'] = datos['nombre']


        if self._datos['altura_par_inicial'] == 0:
            self._datos['altura_par_inicial'] = datos['altura_par_inicial']

        if self._datos['altura_impar_inicial'] == 0:
            self._datos['altura_impar_inicial'] = datos['altura_impar_inicial']

        if not datos['altura_par_final'] == 0:
            self._datos['altura_par_final'] = datos['altura_par_final']

        if not datos['altura_impar_final'] == 0:
            self._datos['altura_impar_final'] = datos['altura_impar_final']

    def add_tiempo(self, tiempo):
        self._tiempo += tiempo

    def get_tiempo(self):
        return self._tiempo

    def add_longitud(self, longitud):
        self._longitud += longitud

    def get_longitud(self):
        return self._longitud

    def get_dato(self, key):
        return self._datos[key]

    def get_datos(self):
        return {'nombre_oficial': self._datos['nombre_oficial'],
                'nombre': self._datos['nombre'],
                'altura_inicial': self._get_altura_inicial(),
                'altura_final': self._get_altura_final(), }

    def get_geometria(self):
        return self._geometria

    def _get_altura_inicial(self):
        if self._datos['altura_par_inicial'] > self._datos['altura_par_final']:
            altura_inicial = max(self._datos['altura_par_inicial'], self._datos['altura_impar_inicial'])
        else:
            altura_inicial = min(self._datos['altura_par_inicial'], self._datos['altura_impar_inicial'])
        return altura_inicial

    def _get_altura_final(self):
        if self._datos['altura_par_final'] > self._datos['altura_par_inicial']:
            altura_final = max(self._datos['altura_par_final'], self._datos['altura_impar_final'])
        else:
            altura_final = min(self._datos['altura_par_final'], self._datos['altura_impar_final'])
        return altura_final

    def concatenar_linea(self, linea):
        self._geometria = self._geometrias_repository.concatenar_lineas(self._geometria, linea)
        return self._geometria