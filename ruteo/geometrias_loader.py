# -*- coding: utf-8 -*-
import csv

from osgeo import ogr

class GeometriasLoader(object):
    def __init__(self):
        pass

    def load(self, srs, file_geometrias):
        geometrias = csv.reader(open(file_geometrias), delimiter=';', quotechar='"')

        result = dict()
        # srs = osr.SpatialReference()
        #srs.ImportFromEPSG(srid)

        for geometria in geometrias:
            ewkt = geometria[3].split(';')
            wkt = ewkt[1]

            #result[(int(geometria[0]), int(geometria[1]))] = ogr.CreateGeometryFromWkt(wkt, srs)

            result[int(geometria[0])] = {
                'nodo_origen': int(geometria[1]),
                'nodo_destino': int(geometria[2]),
                'geometria': ogr.CreateGeometryFromWkt(wkt, srs),
                'longitud': float(geometria[4]),
            }

        return result
