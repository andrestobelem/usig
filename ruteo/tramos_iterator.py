# -*- coding: utf-8 -*-
class TramosIterator(object):
    def __init__(self, tramos):
        self._tramos = tramos
        self._index = 0

    def __iter__(self):
        return self

    def __len__(self):
        return len(self._tramos)

    def next(self):
        if self._index == len(self._tramos):
            self._index = 0
            raise StopIteration
        result = (self._tramos[self._index][0], self._tramos[self._index][1])
        self._index += 1
        return result
