# -*- coding: utf-8 -*-
import csv


class CostosLoader(object):
    def __init__(self):
        pass

    def load(self, file_in):
        costos = csv.reader(open(file_in), delimiter=';', quotechar='"')

        result = dict()

        for costo in costos:
            # result[(int(costo[0]), int(costo[1]))] = float(costo[2])
            result[(costo[0], costo[1])] = float(costo[2])

        return result