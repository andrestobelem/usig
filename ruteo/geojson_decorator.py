# -*- coding: utf-8 -*-
import geojson as gs


class GeoJSONDecorator(object):
    def __init__(self):
        pass

    def decorate(self, srid, recorrido):
        features = list()

        i = 0
        longitud_total = 0
        tiempo_total = 0
        for k, v in sorted(recorrido.iteritems(), key=lambda x: x[1]['orden']):
            propiedades = {}
            if 'datos' in v:
                propiedades = v['datos']

            if 'longitud' in v:
                longitud = v['longitud']
                propiedades.update({'longitud': longitud, })
                longitud_total += longitud

            if 'tiempo' in v:
                tiempo = v['tiempo']
                propiedades.update({'tiempo': tiempo, })
                tiempo_total += tiempo

            if 'cruces' in v:
                cruces = v['cruces']
                propiedades.update({'cruces': cruces, })

            geometria = None
            if 'geometria' in v:
                geometria = gs.loads(v['geometria'].ExportToJson())

            if 'angulo' in v and 'giro' in v:
                angulo = v['angulo']
                propiedades.update({'angulo': angulo, })
                giro = v['giro']
                propiedades.update({'giro': giro, })

            propiedades.update({'orden': v['orden'], })

            features.append(
                gs.Feature(geometry=geometria, id=k, properties=propiedades))

            i += 1

        return gs.FeatureCollection(features,
                                    crs="{'type': 'name', 'properties': {'name': 'EPSG:" + str(srid) + "'}}",
                                    properties={'longitud': longitud_total, 'tiempo_total': tiempo_total})
