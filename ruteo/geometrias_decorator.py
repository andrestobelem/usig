# -*- coding: utf-8 -*-
class GeometriasDecorator(object):
    def __init__(self, geometrias_repository):
        self.geometrias_repository = geometrias_repository

    def decorate(self, dic):
        for key in dic:
            #tramo = self.geometrias_repository.get(key[1])

            geometria = self.geometrias_repository.get_geometria(key[1])

            #print key
            #print tramo
            #if tramo['nodo_destino'] == key[0]:
            #    print "dar vuelta"

            longitud = self.geometrias_repository.get_longitud(key[1])
            dic[key].update({'geometria': geometria, 'longitud': longitud})
        return dic

    def undecorate(self, dic):
        for key in dic:
            dic[key].pop('geometria')
        return dic