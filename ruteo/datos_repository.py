# -*- coding: utf-8 -*-
class DatosRepository(object):
    def __init__(self, datos_loader):
        self._datos_loader = datos_loader
        self._datos = None

    def load(self, file_in):
        self._datos = self._datos_loader.load(file_in)

    def get_dato(self, key):
        return self._datos[key]

    def add_dato(self, key, dato):
        self._datos.update({key: dato})

    def del_dato(self, key):
        del self._datos[key]

    def get_dato_by_calle_altura(self, calle, altura):
        for dato in self._datos.iteritems():
            if dato[1].get('codigo_de_calle') == calle or dato[1].get('nombre_oficial') == calle:
                if ((altura % 2 == 0
                     and dato[1].get('altura_par_inicial') <= altura <= dato[1].get('altura_par_final')) or
                        (altura % 2 == 1
                         and dato[1].get('altura_impar_inicial') <= altura <= dato[1].get('altura_impar_final'))):
                    return dato
        raise Exception('No existe el dato')

    def get_factor(self, key, altura):
        dato = self.get_dato(key)

        if altura % 2 == 0:
            factor = (altura - dato['altura_par_inicial']) / (
                float(dato['altura_par_final'] - dato['altura_par_inicial']))
        else:
            factor = (altura - dato['altura_impar_inicial']) / (
                float(dato['altura_impar_final'] - dato['altura_impar_inicial']))

        return abs(1 - factor)

    def get_altura_inicial(self, key):
        dato = self.get_dato(key)

        if dato['altura_par_inicial'] > dato['altura_par_final']:
            altura_inicial = max(dato['altura_par_inicial'], dato['altura_impar_inicial'])
        else:
            altura_inicial = min(dato['altura_par_inicial'], dato['altura_impar_inicial'])
        return altura_inicial

    def get_altura_final(self, key):
        dato = self.get_dato(key)

        if dato['altura_par_final'] > dato['altura_par_inicial']:
            altura_final = max(dato['altura_par_final'], dato['altura_impar_final'])
        else:
            altura_final = min(dato['altura_par_final'], dato['altura_impar_final'])
        return altura_final

    def get_datos_por_nodo_origen(self, nodo_origen, id_tramo):
        res = list()
        for dato in self._datos.iteritems():
            if dato[1].get('nodo_origen') == nodo_origen and not dato[0] == id_tramo:
                res.append(dato[1].get('nombre_oficial'))
        return res

    def get_datos_por_nodo_destino(self, nodo_destino, id_tramo):
        res = list()
        for dato in self._datos.iteritems():
            if dato[1].get('nodo_destino') == nodo_destino and not dato[0] == id_tramo:
                res.append(dato[1].get('nombre_oficial'))
        return res
