# -*- coding: utf-8 -*-
class DatosDecorator(object):
    def __init__(self, datos_repository):
        self.datos_repository = datos_repository

    def decorate(self, dic):
        for key in dic:
            dic[key].update({'datos': self.datos_repository.get_dato(key[1])})
        return dic

