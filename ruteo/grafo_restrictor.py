# -*- coding: utf-8 -*-
class GrafoRestrictor(object):
    def __init__(self, vertice_converter):
        self._vertice_converter = vertice_converter

    def aplicar_restricciones(self, grafo, restricciones):
        for restriccion in restricciones:

            arista_in = [restriccion[0], restriccion[1]]
            arista_out = [restriccion[1], restriccion[2]]

            vertice_in = self._vertice_converter.crear_vertice_in(arista_in)
            vertice_out = self._vertice_converter.crear_vertice_out(arista_out)

            try:
                grafo.remove_edge(vertice_in, vertice_out)
            except Exception:
                print '(' + vertice_in + ',' + vertice_out + ')'

        return grafo