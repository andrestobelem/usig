# -*- coding: utf-8 -*-
from osgeo import osr


class CoordenadasRepository(object):
    def __init__(self, srid, coordenadas_loader):
        self._coordenadas_loader = coordenadas_loader
        self._coordenadas = None
        self._srid = srid

        self._srs = osr.SpatialReference()
        self._srs.ImportFromEPSG(self._srid)

    def load(self, file_in):
        self._coordenadas = self._coordenadas_loader.load(self._srs, file_in)

    def get_srid(self):
        return self._srid

    def get_nodo_mas_cercano(self, punto):
        d = {k: punto.Distance(v) for k, v in self._coordenadas.iteritems()}
        return min(d, key=d.get)