# -*- coding: utf-8 -*-
import csv


class RestriccionesLoader(object):
    def __init__(self):
        pass

    def load(self, file_in):
        return csv.reader(open(file_in), delimiter=' ', quotechar='"')