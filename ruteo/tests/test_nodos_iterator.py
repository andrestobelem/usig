# -*- coding: utf-8 -*-
import unittest

import ruteo


class NodosIteratorTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def test_iterar_sobre_nodos_dos_veces(self):
        nodos = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        nodos_iterator = ruteo.NodosIterator(nodos)

        tramos = [tramo for tramo in nodos_iterator]

        self.assertEqual(tramos, [(1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (8, 9)])

        tramos = [tramo for tramo in nodos_iterator]

        self.assertEqual(tramos, [(1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (8, 9)])
