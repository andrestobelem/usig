# -*- coding: utf-8 -*-
import unittest

from ruteo.tests import IsIsomorphicMixin, DIR_GRAFOS, DIR_RESULTADOS
import ruteo


class VerticeExpanderTestCase(IsIsomorphicMixin, unittest.TestCase):
    def setUp(self):
        self.grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())
        self.vertice_expander = ruteo.VerticeExpander(ruteo.VerticeConverter())

    def test_expandir_un_vertice(self):
        file_in = '{0}grafo.edgelist'.format(DIR_GRAFOS)
        file_res = '{0}red_un_vertice.edgelist'.format(DIR_RESULTADOS)
        vertice = '5'

        grafo_in = self.grafo_io.load(file_in)
        grafo_out = self.grafo_io.load(file_res)

        self.vertice_expander.expandir_vertice(grafo_in, vertice)

        self.assertIsIsomorphic(grafo_in, grafo_out)

    def test_expandir_todos_los_vertices(self):
        file_in = '{0}grafo.edgelist'.format(DIR_GRAFOS)
        file_res = '{0}red.edgelist'.format(DIR_RESULTADOS)

        grafo_in = self.grafo_io.load(file_in)
        grafo_res = self.grafo_io.load(file_res)

        for v in range(1, 10):
            self.vertice_expander.expandir_vertice(grafo_in, str(v))

        self.assertIsIsomorphic(grafo_in, grafo_res)