# -*- coding: utf-8 -*-
import unittest

from ruteo.tests import IsIsomorphicMixin, DIR_GRAFOS, DIR_REDES, DIR_RESULTADOS, DIR_RESTRICCIONES
import ruteo


class RedVialCreatorTestCase(IsIsomorphicMixin, unittest.TestCase):
    def setUp(self):
        self.red_creator = ruteo.RedCreator()
        self.grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())

    def test_expandir_grafo_simple_mano_y_doble_mano_con_restricciones(self):
        file_in = '{0}grafo_simple_mano_y_doble_mano.edgelist'.format(DIR_GRAFOS)
        file_out = '{0}red_simple_mano_y_doble_mano_con_restricciones.edgelist'.format(DIR_REDES)
        file_res = '{0}red_simple_mano_y_doble_mano_con_restricciones.edgelist'.format(DIR_RESULTADOS)
        file_restricciones = '{0}restricciones_simple_mano_y_doble_mano.csv'.format(DIR_RESTRICCIONES)

        self.red_creator.create(file_in, file_restricciones, file_out)

        grafo_out = self.grafo_io.load(file_out)
        grafo_res = self.grafo_io.load(file_res)

        self.assertIsIsomorphic(grafo_out, grafo_res)

    def test_expandir_grafo_caba_con_restricciones(self):
        file_in = DIR_GRAFOS + 'caba_grafo.edgelist'
        file_out = DIR_REDES + 'caba_red.edgelist'
        file_restricciones = DIR_RESTRICCIONES + 'caba_restricciones.csv'

        self.red_creator.create(file_in, file_restricciones, file_out)

    def test_expandir_grafo_caba_distancia_con_restricciones(self):
        file_in = DIR_GRAFOS + 'caba_grafo_distancia.edgelist'
        file_out = DIR_REDES + 'caba_red_distancia.edgelist'
        file_restricciones = DIR_RESTRICCIONES + 'caba_restricciones.csv'

        self.red_creator.create(file_in, file_restricciones, file_out)

        # def test_expandir_grafo_bsas_con_restricciones(self):
        # file_in = DIR_GRAFOS + 'bsas_grafo.edgelist'
        #     file_out = DIR_REDES + 'bsas_red.edgelist'
        #     file_restricciones = DIR_RESTRICCIONES + 'bsas_restricciones.csv'
        #
        #     self.red_vial_creator.create(file_in, file_restricciones, file_out)