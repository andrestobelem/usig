# -*- coding: utf-8 -*-
import unittest
from osgeo import ogr

from ruteo.tests import DIR_GEOMERIAS
import ruteo


class CoordenadasRepositoryTestCase(unittest.TestCase):
    def setUp(self):
        self.coordenadas_repository = ruteo.CoordenadasRepository(ruteo.SRID_IN, ruteo.CoordenadasLoader())

    def test_nodo_mas_cercano_1(self):
        file_in = '{0}caba_coordenadas.csv'.format(DIR_GEOMERIAS)
        self.coordenadas_repository.load(file_in)

        x = 108028
        y = 101424

        punto = ogr.Geometry(ogr.wkbPoint)
        punto.AddPoint(x, y)

        nodo = self.coordenadas_repository.get_nodo_mas_cercano(punto)

        self.assertEqual(6809, nodo)

    def test_nodo_mas_cercano_2(self):
        file_in = '{0}caba_coordenadas.csv'.format(DIR_GEOMERIAS)
        self.coordenadas_repository.load(file_in)

        x = 98797
        y = 106366

        punto = ogr.Geometry(ogr.wkbPoint)
        punto.AddPoint(x, y)

        nodo = self.coordenadas_repository.get_nodo_mas_cercano(punto)

        self.assertEqual(1986, nodo)