# -*- coding: utf-8 -*-
import unittest

from ruteo.tests import IsIsomorphicMixin, DIR_GRAFOS, DIR_RESULTADOS
import ruteo


class GrafoExpanderTestCase(IsIsomorphicMixin, unittest.TestCase):
    def setUp(self):
        self.grafo_expander = ruteo.GrafoExpander(ruteo.VerticeExpander(ruteo.VerticeConverter()))
        self.grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())

    def test_expandir_grafo(self):
        file_in = '{0}grafo.edgelist'.format(DIR_GRAFOS)
        file_res = '{0}red.edgelist'.format(DIR_RESULTADOS)

        grafo_in = self.grafo_expander.expandir_grafo(self.grafo_io.load(file_in))

        grafo_res = self.grafo_io.load(file_res)
        self.assertIsIsomorphic(grafo_in, grafo_res)

    def test_expandir_grafo_simple_mano(self):
        file_in = '{0}grafo_simple_mano.edgelist'.format(DIR_GRAFOS)
        file_res = '{0}red_simple_mano.edgelist'.format(DIR_RESULTADOS)

        grafo_in = self.grafo_expander.expandir_grafo(self.grafo_io.load(file_in))

        grafo_res = self.grafo_io.load(file_res)
        self.assertIsIsomorphic(grafo_in, grafo_res)

    def test_expandir_grafo_simple_mano_en_te(self):
        file_in = '{0}grafo_simple_mano_en_te.edgelist'.format(DIR_GRAFOS)
        file_res = '{0}red_simple_mano_en_te.edgelist'.format(DIR_RESULTADOS)

        grafo_in = self.grafo_expander.expandir_grafo(self.grafo_io.load(file_in))

        grafo_res = self.grafo_io.load(file_res)
        self.assertIsIsomorphic(grafo_in, grafo_res)

    def test_expandir_grafo_simple_mano_y_doble_mano(self):
        file_in = '{0}grafo_simple_mano_y_doble_mano.edgelist'.format(DIR_GRAFOS)
        file_res = '{0}red_simple_mano_y_doble_mano.edgelist'.format(DIR_RESULTADOS)

        grafo_in = self.grafo_expander.expandir_grafo(self.grafo_io.load(file_in))

        grafo_res = self.grafo_io.load(file_res)
        self.assertIsIsomorphic(grafo_in, grafo_res)

    def test_expandir_grafo_doble_mano_y_doble_mano(self):
        file_in = '{0}grafo_doble_mano_y_doble_mano.edgelist'.format(DIR_GRAFOS)
        file_res = '{0}red_doble_mano_y_doble_mano.edgelist'.format(DIR_RESULTADOS)

        grafo_in = self.grafo_expander.expandir_grafo(self.grafo_io.load(file_in))

        grafo_res = self.grafo_io.load(file_res)
        self.assertIsIsomorphic(grafo_in, grafo_res)