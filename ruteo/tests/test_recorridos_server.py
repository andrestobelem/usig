# -*- coding: utf-8 -*-
import unittest

import ruteo


class RecorridosServerTestCase(unittest.TestCase):
    def setUp(self):
        # self.file_grafo_caba = '{0}caba_grafo.edgelist'.format(DIR_GRAFOS)
        # self.file_red_caba = '{0}caba_red.edgelist'.format(DIR_REDES)
        #
        # self.grafo_loader = ruteo.GrafoLoader()
        #
        # self.grafo_caba = self.grafo_loader.load(self.file_grafo_caba)
        # self.red_caba = self.grafo_loader.load(self.file_red_caba)
        #
        # self.red_searcher = ruteo.RedSearcher(self.grafo_caba, self.red_caba)

        self.recorridos_server = ruteo.RecorridosServerBuilder.get_recorrido_server_auto()

    # def test_recorrido_con_coordenadas(self):
    # origen = 6809
    # destino = 1986
    #
    # origen_x = 108028
    # origen_y = 101424
    #
    #     destino_x = 98797
    #     destino_y = 106366
    #
    #     self.assertEqual(self.red_vial_searcher.buscar_recorrido_cruce(origen, destino),
    #                      self.recorridos_server._get_recorrido_en_auto(origen_x, origen_y, destino_x, destino_y))

    def test_recorrido_con_coordenadas_2(self):
        origen_calle = 17071
        origen_altura = 622
        origen_cruce = None
        destino_calle = 3058
        destino_altura = None
        destino_cruce = 3156
        origen_x = 108142.240571
        origen_y = 101518.637052
        destino_x = 97525.4296875
        destino_y = 106595.34375

        parametros = dict(
            origen_calle=origen_calle,
            origen_altura=origen_altura,
            origen_cruce=origen_cruce,
            destino_calle=destino_calle,
            destino_altura=destino_altura,
            destino_cruce=destino_cruce,
            origen_x=origen_x,
            origen_y=origen_y,
            destino_x=destino_x,
            destino_y=destino_y,
        )

        self.recorridos_server.get_recorrido(**parametros)
