# -*- coding: utf-8 -*-
import unittest

import ruteo


class VerticeConverterTestCase(unittest.TestCase):
    def setUp(self):
        self.vertice_converter = ruteo.VerticeConverter()

    def test_crear_un_vertice_in(self):
        arista_in = ['200', '123']
        vertice_in = '123.200'
        self.assertEqual(vertice_in, self.vertice_converter.crear_vertice_in(arista_in))

    def test_crear_un_vertice_out(self):
        arista_out = ['1234567890', '9876543210']
        vertice_out = '-1234567890.9876543210'
        self.assertEqual(vertice_out, self.vertice_converter.crear_vertice_out(arista_out))

    def test_contraer_vertice_in(self):
        arista_in = ['200', '123']
        vertice_original = '123'
        vertice_in = self.vertice_converter.crear_vertice_in(arista_in)
        self.assertEqual(vertice_original, self.vertice_converter.contraer_vertice(vertice_in))

    def test_contraer_vertice_out(self):
        arista_out = ['1234567890', '9876543210']
        vertice_original = '1234567890'
        vertice_out = self.vertice_converter.crear_vertice_out(arista_out)
        self.assertEqual(vertice_original, self.vertice_converter.contraer_vertice(vertice_out))

    def test_contraer_recorrido(self):
        recorrido = self.vertice_converter.contraer_vertices(
            ['1', '1', '1', '1', '2.5', '2', '-2.3', '2', '1', '1.4', '3', '5', '3', '2', '2', '2', '1', '1'])
        self.assertEqual(recorrido, ['1', '2', '1', '3', '5', '3', '2', '1'])