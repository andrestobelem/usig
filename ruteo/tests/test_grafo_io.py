# -*- coding: utf-8 -*-
import unittest

from ruteo.tests import IsIsomorphicMixin, DIR_GRAFOS
import ruteo


class GrafosIOTestCase(IsIsomorphicMixin, unittest.TestCase):
    def setUp(self):
        self.grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())

    def test_cargar_un_grafo(self):
        file_in = '{0}grafo.edgelist'.format(DIR_GRAFOS)
        grafo = self.grafo_io.load(file_in)

        self.assertNotEqual(grafo, None)

    def test_cargar_y_grabar_un_grafo(self):
        file_in = '{0}grafo.edgelist'.format(DIR_GRAFOS)
        file_out = '{0}grafo_grabado.edgelist'.format(DIR_GRAFOS)

        grafo_in = self.grafo_io.load(file_in)

        self.grafo_io.save(grafo_in, file_out)

        grafo_out = self.grafo_io.load(file_out)

        self.assertIsIsomorphic(grafo_in, grafo_out)

    def test_cargar_el_grafo_de_la_caba(self):
        file_in = '{0}caba_grafo.edgelist'.format(DIR_GRAFOS)
        self.grafo_io.load(file_in)

        # def test_cargar_el_grafo_de_bsas(self):
        # file_in = DIR_GRAFOS + 'bsas_grafo.edgelist'
        #     self.grafo_io.load(file_in)