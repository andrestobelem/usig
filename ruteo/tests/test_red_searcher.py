# -*- coding: utf-8 -*-
import unittest

from ruteo.tests import DIR_GRAFOS, DIR_REDES
import ruteo


class RedVialSearcherTestCase(unittest.TestCase):
    def setUp(self):
        self.file_grafo_caba = '{0}caba_grafo.edgelist'.format(DIR_GRAFOS)
        self.file_red_caba = '{0}caba_red.edgelist'.format(DIR_REDES)

        self.grafo_loader = ruteo.GrafoLoader()

        self.grafo_caba = self.grafo_loader.load(self.file_grafo_caba)
        self.red_caba = self.grafo_loader.load(self.file_red_caba)

        self.red_vial_searcher = ruteo.RedSearcher(self.grafo_caba, self.red_caba)

    def assertEqualRecorrido(self, origen, destino, nodos):
        recorrido = self.red_vial_searcher.buscar_recorrido_cruce(origen, destino)
        self.assertEqual(recorrido, nodos)

    def test_recorrido_en_auto(self):
        file_grafo = '{0}grafo.edgelist'.format(DIR_GRAFOS)
        file_red = '{0}red.edgelist'.format(DIR_REDES)

        grafo = self.grafo_loader.load(file_grafo)
        red = self.grafo_loader.load(file_red)

        red_vial_searcher = ruteo.RedSearcher(grafo, red)

        origen = 1
        destino = 9

        recorrido = red_vial_searcher.buscar_recorrido_cruce(origen, destino)

        self.assertEqual(recorrido, [1, 4, 5, 6, 9])

    def test_recorrido_en_auto_caba(self):
        origen = 6023
        destino = 5971
        nodos = [6023, 5999, 5988, 5971]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_en_auto_caba_inverso(self):
        origen = 5971
        destino = 6023
        nodos = [5971, 6147, 6159, 6182, 6183, 6023]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_en_auto_pasando_por_manos_simples(self):
        origen = 6796
        destino = 7470
        nodos = [6796, 6786, 6958, 6959, 6984, 7153, 7307, 7469, 7470]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_en_auto_pasando_por_doble_mano_y_giro_a_izquierda(self):
        origen = 8076
        destino = 7767
        nodos = [8076, 8075, 7917, 7713, 7748, 7767]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_tiene_que_encontrar_un_recorrido_derecho(self):
        origen = 8205
        destino = 6560
        nodos = [8205, 8030, 7913, 7862, 7808, 7677, 7515, 7347, 7184, 7057, 6875, 6688, 6560]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_tiene_que_encontrar_un_recorrido_derecho_volviendo(self):
        origen = 6560
        destino = 8205
        nodos = [6560, 6593, 6694, 6886, 7072, 7197, 7366, 7545, 7686, 7879, 8046, 8209, 8205]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_corto(self):
        origen = 6796
        destino = 6183
        nodos = [6796, 6609, 6443, 6287, 6133, 6147, 6159, 6182, 6183]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_corto_volviendo(self):
        origen = 6183
        destino = 6796
        nodos = [6183, 17146, 17129, 6344, 6502, 6530, 6656, 6655, 6654, 6653, 6643, 6819, 6814, 6809, 6796]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_mediano(self):
        origen = 6796
        destino = 3962
        nodos = [6796, 6609, 6443, 6468, 6469, 6490, 6491, 6494, 6502, 6530, 6531, 6363, 6210, 6054, 5880, 5796, 5824,
                 5834, 5835, 5780, 5794, 5812, 16940, 5813, 5791, 5641, 5488, 5342, 5190, 5023, 4860, 4708, 4557, 4456,
                 4313, 4184, 4185, 4170, 4171, 4133, 4134, 4082, 4049, 4050, 3962]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_mediano_volviendo(self):
        origen = 3962
        destino = 6796
        nodos = [3962, 4050, 4049, 4082, 4134, 4133, 4171, 4170, 4185, 4184, 4181, 4312, 4443, 4551, 4556, 4696, 4692,
                 4687, 4686, 4680, 4815, 4672, 4661, 4814, 4975, 5122, 5115, 5108, 5116, 5135, 5251, 5257, 5259, 5345,
                 5367, 5434, 5500, 5664, 5747, 5816, 5971, 6147, 6308, 6468, 6614, 6809, 6796]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_largo(self):
        origen = 6183
        destino = 2260
        nodos = [6183, 6023, 5855, 5769, 17144, 17139, 5795, 5796, 5824, 5834, 5835, 5780, 5794, 5812, 16940, 5813,
                 5791, 5641, 5488, 5342, 5190, 5023, 5197, 5040, 5041, 5046, 5047, 5055, 5067, 5065, 5060, 5053, 5028,
                 5006, 5001, 4943, 4870, 4807, 4752, 4695, 4655, 4731, 13184, 4547, 4477, 4529, 4344, 4292, 4241, 4202,
                 4153, 4086, 4037, 3974, 3892, 3835, 3770, 3719, 3654, 3585, 3517, 3502, 3506, 3414, 3344, 3250, 3172,
                 3110, 3033, 3016, 2940, 2886, 2835, 2801, 2770, 2740, 2698, 2647, 2599, 2584, 2552, 2546, 2539, 2535,
                 2533, 2528, 2523, 2476, 2491, 2480, 2465, 2462, 269, 2414, 2350, 2260]
        self.assertEqualRecorrido(origen, destino, nodos)

    def test_recorrido_largo_volviendo(self):
        origen = 6183
        destino = 2260
        nodos = [6183, 6023, 5855, 5769, 17144, 17139, 5795, 5796, 5824, 5834, 5835, 5780, 5794, 5812, 16940, 5813,
                 5791, 5641, 5488, 5342, 5190, 5023, 5197, 5040, 5041, 5046, 5047, 5055, 5067, 5065, 5060, 5053, 5028,
                 5006, 5001, 4943, 4870, 4807, 4752, 4695, 4655, 4731, 13184, 4547, 4477, 4529, 4344, 4292, 4241, 4202,
                 4153, 4086, 4037, 3974, 3892, 3835, 3770, 3719, 3654, 3585, 3517, 3502, 3506, 3414, 3344, 3250, 3172,
                 3110, 3033, 3016, 2940, 2886, 2835, 2801, 2770, 2740, 2698, 2647, 2599, 2584, 2552, 2546, 2539, 2535,
                 2533, 2528, 2523, 2476, 2491, 2480, 2465, 2462, 269, 2414, 2350, 2260]
        self.assertEqualRecorrido(origen, destino, nodos)
