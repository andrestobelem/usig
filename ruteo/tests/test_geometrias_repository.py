# -*- coding: utf-8 -*-
import unittest
from osgeo import ogr

from ruteo.tests import DIR_GEOMERIAS
import ruteo


class GeometriasRepositoryTestCase(unittest.TestCase):
    def setUp(self):
        self.geometrias_repository = ruteo.GeometriasRepository(ruteo.SRID_IN, ruteo.GeometriasLoader())

    def test_tramo_mas_cercano_1(self):
        file_in = '{0}caba_geometrias.csv'.format(DIR_GEOMERIAS)
        self.geometrias_repository.load(file_in)

        x = 108028
        y = 101424

        punto = ogr.Geometry(ogr.wkbPoint)
        punto.AddPoint(x, y)

        tramo = self.geometrias_repository.get_tramo_mas_cercano(punto)

        self.assertEqual((6984, 6809), tramo)

    def test_tramo_mas_cercano_2(self):
        file_in = '{0}caba_geometrias.csv'.format(DIR_GEOMERIAS)
        self.geometrias_repository.load(file_in)

        x = 98797
        y = 106366

        punto = ogr.Geometry(ogr.wkbPoint)
        punto.AddPoint(x, y)

        tramo = self.geometrias_repository.get_tramo_mas_cercano(punto)
        self.assertEqual((1931, 1986), tramo)

    def test_segmento_mas_cercano(self):
        file_in = '{0}caba_geometrias.csv'.format(DIR_GEOMERIAS)
        self.geometrias_repository.load(file_in)

        x = 108351.81836
        y = 101787.78410

        punto = ogr.Geometry(ogr.wkbPoint)
        punto.AddPoint(x, y)

        tramo = self.geometrias_repository.get_tramo_mas_cercano(punto)

        segmento = self.geometrias_repository.get_segmento_mas_cercano(tramo, punto)

        self.assertEqual(((108386.8828125, 101732.21875), (108380.8125, 101860.046875)), segmento)

    def test_punto_mas_cercano(self):
        file_in = '{0}caba_geometrias.csv'.format(DIR_GEOMERIAS)
        self.geometrias_repository.load(file_in)

        x = 107976.24570
        y = 101948.12685

        punto = ogr.Geometry(ogr.wkbPoint)
        punto.AddPoint(x, y)

        tramo = self.geometrias_repository.get_tramo_mas_cercano(punto)

        segmento = self.geometrias_repository.get_segmento_mas_cercano(tramo, punto)

        punto_mas_cercano = self.geometrias_repository.get_punto_mas_cercano(segmento, punto)

        # geometria = self.geometrias_repository.get_geometria(tramo)

        p1 = ogr.Geometry(ogr.wkbPoint)
        p1.AddPoint(punto_mas_cercano[0], punto_mas_cercano[1])

        self.assertEqual((107976.68864865055, 101947.73403548055), punto_mas_cercano)

    def test_cortar_linea(self):
        pass




