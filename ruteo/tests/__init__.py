# -*- coding: utf-8 -*-
import networkx as nx
import networkx.algorithms.isomorphism as iso

DIR_GRAFOS = 'data/tests/grafos/'
DIR_REDES = 'data/tests/redes/'
DIR_RESULTADOS = 'data/tests/resultados/'
DIR_RESTRICCIONES = 'data/tests/restricciones/'
DIR_GEOMERIAS = 'data/tests/geometrias/'
DIR_DATOS = 'data/tests/datos/'


class IsIsomorphicMixin(object):
    def assertIsIsomorphic(self, un_grafo, otro_grafo):
        self.assertTrue(nx.is_isomorphic(un_grafo, otro_grafo, edge_match=iso.categorical_edge_match('weight', None)))