# -*- coding: utf-8 -*-
from osgeo import ogr

import ruteo


class RecorridosServer(object):
    #def __init__(self, coordenadas_repository, geometrias_repository, datos_repository):
    def __init__(self, geometrias_repository, datos_repository):
        #self._coordenadas_repository = coordenadas_repository
        self._geometrias_repository = geometrias_repository
        self._datos_repository = datos_repository

        self._geometrias_decorator = ruteo.GeometriasDecorator(self._geometrias_repository)
        self._datos_decorator = ruteo.DatosDecorator(self._datos_repository)
        self._tiempos_decorator = ruteo.TiemposDecorator()
        self._segmentos_decorator = ruteo.SegmentosDecorator(self._geometrias_repository, datos_repository)
        self._srid_decorator = ruteo.SridDecorator(self._geometrias_repository)
        self._geojson_decorator = ruteo.GeoJSONDecorator()

    def get_recorrido(self,
                      origen_calle=None,
                      origen_altura=None,
                      origen_cruce=None,
                      destino_calle=None,
                      destino_altura=None,
                      destino_cruce=None,
                      origen_x=None,
                      origen_y=None,
                      destino_x=None,
                      destino_y=None,
                      srid_from=ruteo.SRID_FROM,
                      srid_to=ruteo.SRID_TO,
                      red=None,
                      autopistas=False,
                      distancia=False,
                      cortes=True,
                      origen_geocoder=False,
                      destino_geocoder=False):

        origen_punto = ogr.Geometry(ogr.wkbPoint)
        origen_punto.AddPoint(origen_x, origen_y)

        destino_punto = ogr.Geometry(ogr.wkbPoint)
        destino_punto.AddPoint(destino_x, destino_y)

        if origen_geocoder:
            origen_punto = self._geometrias_repository.transform_in(ruteo.SRID_GEOCODER, origen_punto)
        else:
            origen_punto = self._geometrias_repository.transform_in(srid_from, origen_punto)

        if destino_geocoder:
            destino_punto = self._geometrias_repository.transform_in(ruteo.SRID_GEOCODER, destino_punto)
        else:
            destino_punto = self._geometrias_repository.transform_in(srid_from, destino_punto)

        origen_tramo = self._geometrias_repository.get_tramo_mas_cercano(origen_punto)
        destino_tramo = self._geometrias_repository.get_tramo_mas_cercano(destino_punto)

        if distancia:
            distancia = 'distancia'
        else:
            distancia = 'tiempo'

        db = ruteo.get_db()
        cur = db.cursor()
        q = """SELECT * FROM ruteo_osm.pg_routing_ss(%d,%d,'%s','%s')""" % (origen_tramo, destino_tramo, red, distancia)
        #print q
        cur.execute(q)
        rows = cur.fetchall()

        tramos = {(row[0], row[1]): {'orden': orden, 'costo': row[2]} for row, orden in zip(rows, range(0, len(rows)))}

        #print tramos

        result = self._segmentos_decorator.decorate(
            self._tiempos_decorator.decorate(
                self._datos_decorator.decorate(
                    self._geometrias_decorator.decorate(tramos))))

        result = self._geojson_decorator.decorate(srid_to, self._srid_decorator.decorate(srid_to, result))

        return result

    def get_recorrido_bis(self,
                          origen_calle=None,
                          origen_altura=None,
                          origen_cruce=None,
                          destino_calle=None,
                          destino_altura=None,
                          destino_cruce=None,
                          origen_x=None,
                          origen_y=None,
                          destino_x=None,
                          destino_y=None,
                          srid_from=ruteo.SRID_FROM,
                          srid_to=ruteo.SRID_TO):

        origen_punto = ogr.Geometry(ogr.wkbPoint)
        origen_punto.AddPoint(origen_x, origen_y)

        destino_punto = ogr.Geometry(ogr.wkbPoint)
        destino_punto.AddPoint(destino_x, destino_y)

        origen_punto = self._geometrias_repository.transform_in(srid_from, origen_punto)
        destino_punto = self._geometrias_repository.transform_in(srid_from, destino_punto)

        if origen_cruce is not None:
            origen = self._coordenadas_repository.get_nodo_mas_cercano(origen_punto)
        else:
            origen = 'origen'
            origen_dato_calle_altura = self._datos_repository.get_dato_by_calle_altura(origen_calle, origen_altura)
            origen_nodo_0 = origen_dato_calle_altura[0][0]
            origen_nodo_1 = origen_dato_calle_altura[0][1]

            origen_vereda = 'par' if origen_altura % 2 == 0 else 'impar'

            if origen_dato_calle_altura[1]['sentido'] == 2:
                if self._datos_repository.get_dato((origen_nodo_1, origen_nodo_0))['vereda'] == origen_vereda:
                    origen_arista = (origen_nodo_1, origen_nodo_0)

                elif self._datos_repository.get_dato((origen_nodo_0, origen_nodo_1))['vereda'] == origen_vereda:
                    origen_arista = (origen_nodo_1, origen_nodo_0)

            else:
                if self._grafo.has_edge(origen_nodo_1, origen_nodo_0):
                    origen_arista = (origen_nodo_1, origen_nodo_0)

                elif self._grafo.has_edge(origen_nodo_0, origen_nodo_1):
                    origen_arista = (origen_nodo_0, origen_nodo_1)

            origen_dato = self._datos_repository.get_dato(origen_arista).copy()
            self._cortar_origen(origen, origen_altura, origen_punto, origen_dato, origen_arista)

        if destino_cruce is not None:
            destino = self._coordenadas_repository.get_nodo_mas_cercano(destino_punto)
        else:
            destino = 'destino'
            destino_dato_calle_altura = self._datos_repository.get_dato_by_calle_altura(destino_calle, destino_altura)
            destino_nodo_0 = destino_dato_calle_altura[0][0]
            destino_nodo_1 = destino_dato_calle_altura[0][1]
            destino_vereda = 'par' if destino_altura % 2 == 0 else 'impar'

            if destino_dato_calle_altura[1]['sentido'] == 2:
                if self._datos_repository.get_dato((destino_nodo_1, destino_nodo_0))['vereda'] == destino_vereda:
                    destino_arista = (destino_nodo_0, destino_nodo_1)

                elif self._datos_repository.get_dato((destino_nodo_0, destino_nodo_1))['vereda'] == destino_vereda:
                    destino_arista = (destino_nodo_1, destino_nodo_0)

            else:
                if self._grafo.has_edge(destino_nodo_1, destino_nodo_0):
                    destino_arista = (destino_nodo_1, destino_nodo_0)

                elif self._grafo.has_edge(destino_nodo_0, destino_nodo_1):
                    destino_arista = (destino_nodo_0, destino_nodo_1)

            destino_dato = self._datos_repository.get_dato(destino_arista).copy()
            self._cortar_destino(destino, destino_altura, destino_punto, destino_dato, destino_arista)

        if origen_cruce is None and destino_cruce is None and origen_dato_calle_altura == destino_dato_calle_altura \
                and ((origen_dato_calle_altura[1]['sentido'] == 2 and origen_vereda == destino_vereda) or not
                    origen_dato_calle_altura[1]['sentido'] == 2):
            self._borrar_origen(origen, origen_arista)
            self._borrar_destino(destino, destino_arista)

            origen_vertice_in = self._vertice_converter.crear_vertice_out(origen_arista)
            origen_vertice_out = self._vertice_converter.crear_vertice_in(origen_arista)

            origen_factor = self._datos_repository.get_factor(origen_arista, origen_altura)

            destino_vertice_in = self._vertice_converter.crear_vertice_out(destino_arista)
            destino_vertice_out = self._vertice_converter.crear_vertice_in(destino_arista)

            destino_factor = self._datos_repository.get_factor(destino_arista, destino_altura)

            factor = abs(destino_factor - origen_factor)

            origen_costo = factor * self._red[origen_vertice_in][origen_vertice_out]['weight']
            destino_costo = factor * self._red[destino_vertice_in][destino_vertice_out]['weight']

            origen_geometry_aux = self._geometrias_repository.get_geometria(
                (origen_arista[0], origen_arista[1])).Clone()
            origen_geometry_aux = self._geometrias_repository.cortar_geometria(origen_geometry_aux,
                                                                               origen_punto,
                                                                               destino_punto)

            destino_geometry_aux = self._geometrias_repository.get_geometria(
                (destino_arista[0], destino_arista[1])).Clone()
            destino_geometry_aux = self._geometrias_repository.cortar_geometria(destino_geometry_aux,
                                                                                destino_punto,
                                                                                origen_punto)

            self._grafo.add_edge(origen, destino, weight=origen_costo)
            self._red.add_edge(origen, destino, weight=origen_costo)

            origen_dato.update({'altura_par_inicial': origen_altura})
            origen_dato.update({'altura_impar_inicial': origen_altura})

            destino_dato.update({'altura_par_final': destino_altura})
            destino_dato.update({'altura_impar_final': destino_altura})

            self._geometrias_repository.add_geometria((origen, destino), origen_geometry_aux)
            self._datos_repository.add_dato((origen, destino), origen_dato)
            self._costos_repository.add_costo((origen, destino), origen_costo)

        recorrido = self._red_searcher.buscar_recorrido_cruce(origen, destino)

        nodos_iter = ruteo.NodosIterator(recorrido)

        tramos = {tramo: {'orden': orden} for tramo, orden in zip(nodos_iter, range(0, len(nodos_iter)))}

        result = self._segmentos_decorator.decorate(
            self._tiempos_decorator.decorate(
                self._datos_decorator.decorate(
                    self._geometrias_decorator.decorate(tramos))))

        result = self._geojson_decorator.decorate(srid_to, self._srid_decorator.decorate(srid_to, result))

        if origen_cruce is None and destino_cruce is None and origen_dato_calle_altura == destino_dato_calle_altura \
                and ((origen_dato_calle_altura[1]['sentido'] == 2 and origen_vereda == destino_vereda) or not
                    origen_dato_calle_altura[1]['sentido'] == 2):

            self._grafo.remove_node(origen)
            self._grafo.remove_node(destino)

            self._geometrias_repository.del_geometria((origen, destino))
            self._datos_repository.del_dato((origen, destino))
            self._costos_repository.del_costo((origen, destino))

        else:
            if not (origen_cruce is not None):
                self._borrar_origen(origen, origen_arista)

            if not (destino_cruce is not None):
                self._borrar_destino(destino, destino_arista)

        return result

    def _cortar_origen(self, origen, origen_altura, origen_punto, origen_dato, origen_arista):
        origen_vertice_in = self._vertice_converter.crear_vertice_in(origen_arista)
        origen_vertice_out = self._vertice_converter.crear_vertice_out(origen_arista)

        origen_factor = self._datos_repository.get_factor(origen_arista, origen_altura)

        origen_costo = origen_factor * self._red[origen_vertice_in][origen_vertice_out]['weight']

        origen_geometry_aux = self._geometrias_repository.get_geometria(
            (origen_arista[0], origen_arista[1])).Clone()
        origen_geometry_aux = self._geometrias_repository.cortar_geometria_origen(origen_geometry_aux, origen_punto)

        self._grafo.add_edge(origen, origen_arista[1], weight=origen_costo)
        self._red.add_edge(origen, origen_vertice_out, weight=origen_costo)

        origen_dato.update({'altura_par_inicial': origen_altura})
        origen_dato.update({'altura_impar_inicial': origen_altura})

        self._geometrias_repository.add_geometria((origen, origen_arista[1]), origen_geometry_aux)
        self._datos_repository.add_dato((origen, origen_arista[1]), origen_dato)
        self._costos_repository.add_costo((origen, origen_arista[1]), origen_costo)

    def _cortar_destino(self, destino, destino_altura, destino_punto, destino_dato, destino_arista):
        destino_vertice_in = self._vertice_converter.crear_vertice_in(destino_arista)
        destino_vertice_out = self._vertice_converter.crear_vertice_out(destino_arista)

        destino_factor = self._datos_repository.get_factor(destino_arista, destino_altura)

        destino_costo = destino_factor * self._red[destino_vertice_in][destino_vertice_out]['weight']

        destino_geometry_aux = self._geometrias_repository.get_geometria(
            (destino_arista[0], destino_arista[1])).Clone()
        destino_geometry_aux = self._geometrias_repository.cortar_geometria_destino(destino_geometry_aux, destino_punto)

        self._grafo.add_edge(destino_arista[0], destino, weight=destino_costo)
        self._red.add_edge(destino_vertice_in, destino, weight=destino_costo)

        destino_dato.update({'altura_par_final': destino_altura})
        destino_dato.update({'altura_impar_final': destino_altura})

        self._geometrias_repository.add_geometria((destino_arista[0], destino), destino_geometry_aux)
        self._datos_repository.add_dato((destino_arista[0], destino), destino_dato)
        self._costos_repository.add_costo((destino_arista[0], destino), destino_costo)

    def _borrar_origen(self, origen, origen_arista):
        try:
            self._grafo.remove_node(origen)
            self._red.remove_node(origen)
        except Exception:
            pass

        self._geometrias_repository.del_geometria((origen, origen_arista[1]))
        self._datos_repository.del_dato((origen, origen_arista[1]))
        self._costos_repository.del_costo((origen, origen_arista[1]))

    def _borrar_destino(self, destino, destino_arista):
        try:
            self._grafo.remove_node(destino)
            self._red.remove_node(destino)
        except Exception:
            pass

        self._geometrias_repository.del_geometria((destino_arista[0], destino))
        self._datos_repository.del_dato((destino_arista[0], destino))
        self._costos_repository.del_costo((destino_arista[0], destino))

    def get_recorrido_pie(self,
                          origen_calle=None,
                          origen_altura=None,
                          origen_cruce=None,
                          destino_calle=None,
                          destino_altura=None,
                          destino_cruce=None,
                          origen_x=None,
                          origen_y=None,
                          destino_x=None,
                          destino_y=None,
                          srid_from=ruteo.SRID_FROM,
                          srid_to=ruteo.SRID_TO):

        origen_punto = ogr.Geometry(ogr.wkbPoint)
        origen_punto.AddPoint(origen_x, origen_y)

        destino_punto = ogr.Geometry(ogr.wkbPoint)
        destino_punto.AddPoint(destino_x, destino_y)

        origen_punto = self._geometrias_repository.transform_in(srid_from, origen_punto)
        destino_punto = self._geometrias_repository.transform_in(srid_from, destino_punto)

        if origen_cruce is not None:
            origen = self._coordenadas_repository.get_nodo_mas_cercano(origen_punto)
        else:
            origen = 'origen'
            origen_dato_calle_altura = self._datos_repository.get_dato_by_calle_altura(origen_calle, origen_altura)
            origen_nodo_0 = origen_dato_calle_altura[0][0]
            origen_nodo_1 = origen_dato_calle_altura[0][1]

            origen_arista_ida = (origen_nodo_0, origen_nodo_1)
            origen_arista_vuelta = (origen_nodo_1, origen_nodo_0)

            origen_dato_ida = self._datos_repository.get_dato(origen_arista_ida).copy()
            origen_dato_vuelta = self._datos_repository.get_dato(origen_arista_vuelta).copy()

            self._cortar_origen(origen, origen_altura, origen_punto, origen_dato_ida, origen_arista_ida)
            self._cortar_origen(origen, origen_altura, origen_punto, origen_dato_vuelta, origen_arista_vuelta)

        if destino_cruce is not None:
            destino = self._coordenadas_repository.get_nodo_mas_cercano(destino_punto)
        else:
            destino = 'destino'
            destino_dato_calle_altura = self._datos_repository.get_dato_by_calle_altura(destino_calle, destino_altura)
            destino_nodo_0 = destino_dato_calle_altura[0][0]
            destino_nodo_1 = destino_dato_calle_altura[0][1]

            destino_arista_ida = (destino_nodo_0, destino_nodo_1)
            destino_arista_vuelta = (destino_nodo_1, destino_nodo_0)

            destino_dato_ida = self._datos_repository.get_dato(destino_arista_ida).copy()
            destino_dato_vuelta = self._datos_repository.get_dato(destino_arista_vuelta).copy()

            self._cortar_destino(destino, destino_altura, destino_punto, destino_dato_ida, destino_arista_ida)
            self._cortar_destino(destino, destino_altura, destino_punto, destino_dato_vuelta, destino_arista_vuelta)

        if origen_cruce is None and destino_cruce is None and origen_dato_calle_altura == destino_dato_calle_altura:
            self._borrar_origen(origen, origen_arista_ida)
            self._borrar_origen(origen, origen_arista_vuelta)
            self._borrar_destino(destino, destino_arista_ida)
            self._borrar_destino(destino, destino_arista_vuelta)

            origen_vertice_in_ida = self._vertice_converter.crear_vertice_out(origen_arista_ida)
            origen_vertice_out_ida = self._vertice_converter.crear_vertice_in(origen_arista_ida)

            origen_factor_ida = self._datos_repository.get_factor(origen_arista_ida, origen_altura)

            # destino_vertice_in_ida = self._vertice_converter.crear_vertice_out(destino_arista_ida)
            # destino_vertice_out_ida = self._vertice_converter.crear_vertice_in(destino_arista_ida)

            # destino_factor_ida = self._datos_repository.get_factor(destino_arista_ida, destino_altura)
            destino_factor_ida = self._datos_repository.get_factor(destino_arista_ida, destino_altura)

            factor_ida = abs(destino_factor_ida - origen_factor_ida)

            origen_costo_ida = factor_ida * self._red[origen_vertice_in_ida][origen_vertice_out_ida]['weight']
            # destino_costo_ida = factor_ida * self._red[destino_vertice_in_ida][destino_vertice_out_ida]['weight']

            origen_geometry_aux_ida = self._geometrias_repository.get_geometria(
                (origen_arista_ida[0], origen_arista_ida[1])).Clone()
            origen_geometry_aux_ida = self._geometrias_repository.cortar_geometria(origen_geometry_aux_ida,
                                                                                   origen_punto,
                                                                                   destino_punto)

            #destino_geometry_aux_ida = self._geometrias_repository.get_geometria(
            #    (destino_arista_ida[0], destino_arista_ida[1])).Clone()
            #destino_geometry_aux_ida = self._geometrias_repository.cortar_geometria(destino_geometry_aux_ida,
            #                                                                        destino_punto,
            #                                                                        origen_punto)

            self._grafo.add_edge(origen, destino, weight=origen_costo_ida)
            self._red.add_edge(origen, destino, weight=origen_costo_ida)

            origen_dato_ida.update({'altura_par_inicial': origen_altura})
            origen_dato_ida.update({'altura_impar_inicial': origen_altura})

            origen_dato_ida.update({'altura_par_final': destino_altura})
            origen_dato_ida.update({'altura_impar_final': destino_altura})

            #destino_dato_ida.update({'altura_par_final': destino_altura})
            #destino_dato_ida.update({'altura_impar_final': destino_altura})

            self._geometrias_repository.add_geometria((origen, destino), origen_geometry_aux_ida)
            self._datos_repository.add_dato((origen, destino), origen_dato_ida)
            self._costos_repository.add_costo((origen, destino), origen_costo_ida)

        recorrido = self._red_searcher.buscar_recorrido_cruce(origen, destino)

        nodos_iter = ruteo.NodosIterator(recorrido)

        tramos = {tramo: {'orden': orden} for tramo, orden in zip(nodos_iter, range(0, len(nodos_iter)))}

        result = self._segmentos_decorator.decorate(
            self._tiempos_decorator.decorate(
                self._datos_decorator.decorate(
                    self._geometrias_decorator.decorate(tramos))))

        result = self._geojson_decorator.decorate(srid_to, self._srid_decorator.decorate(srid_to, result))

        if origen_cruce is None and destino_cruce is None and origen_dato_calle_altura == destino_dato_calle_altura:

            try:
                self._grafo.remove_node(origen)
                self._red.remove_node(origen)
            except:
                pass

            try:
                self._grafo.remove_node(destino)
                self._red.remove_node(destino)
            except:
                pass

            self._geometrias_repository.del_geometria((origen, destino))
            self._datos_repository.del_dato((origen, destino))
            self._costos_repository.del_costo((origen, destino))

        else:
            if not (origen_cruce is not None):
                self._borrar_origen(origen, origen_arista_ida)
                self._borrar_origen(origen, origen_arista_vuelta)

            if not (destino_cruce is not None):
                self._borrar_destino(destino, destino_arista_ida)
                self._borrar_destino(destino, destino_arista_vuelta)

        return result

    def get_recorrido_pie_lite(self,
                               origen_x=None,
                               origen_y=None,
                               destino_x=None,
                               destino_y=None,
                               srid_from=ruteo.SRID_FROM,
                               srid_to=ruteo.SRID_TO):

        origen_punto = ogr.Geometry(ogr.wkbPoint)
        origen_punto.AddPoint(origen_x, origen_y)

        destino_punto = ogr.Geometry(ogr.wkbPoint)
        destino_punto.AddPoint(destino_x, destino_y)

        origen_punto = self._geometrias_repository.transform_in(srid_from, origen_punto)
        destino_punto = self._geometrias_repository.transform_in(srid_from, destino_punto)

        origen = self._coordenadas_repository.get_nodo_mas_cercano(origen_punto)

        destino = self._coordenadas_repository.get_nodo_mas_cercano(destino_punto)

        recorrido = self._red_searcher.buscar_recorrido(origen, destino)

        nodos_iter = ruteo.NodosIterator(recorrido)

        tramos = {tramo: {'orden': orden} for tramo, orden in zip(nodos_iter, range(0, len(nodos_iter)))}

        result = self._segmentos_decorator.decorate(
            self._tiempos_decorator.decorate(
                self._datos_decorator.decorate(
                    self._geometrias_decorator.decorate(tramos))))

        result = self._geojson_decorator.decorate(srid_to, self._srid_decorator.decorate(srid_to, result))

        return result

    def get_recorrido_auto_bsas(self,
                                origen=None,
                                destino=None, ):

        recorrido = self._red_searcher.buscar_recorrido_cruce(origen, destino)

        nodos_iter = ruteo.NodosIterator(recorrido)

        tramos = {tramo: {'orden': orden} for tramo, orden in zip(nodos_iter, range(0, len(nodos_iter)))}

        # result = self._segmentos_decorator.decorate(
        # self._tiempos_decorator.decorate(
        # self._datos_decorator.decorate(
        # self._geometrias_decorator.decorate(tramos))))

        # result = self._geojson_decorator.decorate(srid_to, self._srid_decorator.decorate(srid_to, result))

        result = recorrido
        return result