# -*- coding: utf-8 -*-
class CostosRepository(object):
    def __init__(self, costos_loader):
        self._costos_loader = costos_loader
        self._costos = None

    def load(self, file_in):
        self._costos = self._costos_loader.load(file_in)

    def get_costo(self, key):
        return self._costos[key]

    def add_costo(self, key, costo):
        self._costos.update({key: costo})

    def del_costo(self, key):
        del self._costos[key]