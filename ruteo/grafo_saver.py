# -*- coding: utf-8 -*-
import networkx as nx


class GrafoSaver(object):
    def __init__(self):
        pass

    def save(self, grafo, file_out):
        nx.write_weighted_edgelist(grafo, file_out)