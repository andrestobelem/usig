# -*- coding: utf-8 -*-
class NodosIterator(object):
    def __init__(self, nodos):
        self._nodos = nodos
        self._index = 0

    def __iter__(self):
        return self

    def __len__(self):
        return len(self._nodos)

    def next(self):
        if self._index == len(self._nodos) - 1:
            self._index = 0
            raise StopIteration
        result = (self._nodos[self._index], self._nodos[self._index + 1])
        self._index += 1
        return result
