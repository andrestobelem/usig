# -*- coding: utf-8 -*-
class VerticeExpander(object):
    def __init__(self, vertice_converter):
        self._vertice_converter = vertice_converter

    def expandir_vertice(self, grafo, vertice):
        aristas_in = list()
        in_edges = grafo.in_edges(vertice, data=True)

        for arista_in in in_edges:
            vertice_in = self._vertice_converter.crear_vertice_in(arista_in)

            grafo.add_edge(arista_in[0], vertice_in, arista_in[2])

            aristas_in.append((arista_in[0], vertice_in))

            grafo.remove_edge(arista_in[0], arista_in[1])

        aristas_out = list()
        out_edges = grafo.out_edges(vertice, data=True)

        for arista_out in out_edges:
            vertice_out = self._vertice_converter.crear_vertice_out(arista_out)

            grafo.add_edge(vertice_out, arista_out[1], arista_out[2])

            aristas_out.append((vertice_out, arista_out[1]))

            grafo.remove_edge(arista_out[0], arista_out[1])

        for arista_in in aristas_in:
            for arista_out in aristas_out:
                grafo.add_edge(arista_in[1], arista_out[0], weight=0)

        grafo.remove_node(vertice)

        return grafo