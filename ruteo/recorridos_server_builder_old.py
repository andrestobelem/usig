# -*- coding: utf-8 -*-
import ruteo


class RecorridosServerBuilder(object):
    def __init__(self):
        pass

    @staticmethod
    def get_recorrido_server_auto():
        coordenadas_repository = ruteo.CoordenadasRepository(ruteo.SRID_IN, ruteo.CoordenadasLoader())
        coordenadas_repository.load('{0}caba_coordenadas.csv'.format(ruteo.DIR_GEOMETRIAS))

        geometrias_repository = ruteo.GeometriasRepository(ruteo.SRID_IN, ruteo.GeometriasLoader())
        geometrias_repository.load('{0}caba_geometrias.csv'.format(ruteo.DIR_GEOMETRIAS))

        datos_repository = ruteo.DatosRepository(ruteo.DatosLoader())
        datos_repository.load('{0}caba_datos.csv'.format(ruteo.DIR_DATOS))

        costos_repository = ruteo.CostosRepository(ruteo.CostosLoader())
        costos_repository.load('{0}caba_costos.csv'.format(ruteo.DIR_DATOS))

        grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())

        # grafo = grafo_io.load('{0}caba_grafo.edgelist'.format(ruteo.DIR_GRAFOS))
        red = grafo_io.load('{0}caba_red.edgelist'.format(ruteo.DIR_REDES))

        vertice_converter = ruteo.VerticeConverter()

        return ruteo.RecorridosServer(coordenadas_repository, geometrias_repository, datos_repository,
                                      costos_repository,
                                      red, red, vertice_converter)

    @staticmethod
    def get_recorrido_server_auto_distancia():
        coordenadas_repository = ruteo.CoordenadasRepository(ruteo.SRID_IN, ruteo.CoordenadasLoader())
        coordenadas_repository.load('{0}caba_coordenadas.csv'.format(ruteo.DIR_GEOMETRIAS))

        geometrias_repository = ruteo.GeometriasRepository(ruteo.SRID_IN, ruteo.GeometriasLoader())
        geometrias_repository.load('{0}caba_geometrias.csv'.format(ruteo.DIR_GEOMETRIAS))

        datos_repository = ruteo.DatosRepository(ruteo.DatosLoader())
        datos_repository.load('{0}caba_datos.csv'.format(ruteo.DIR_DATOS))

        costos_repository = ruteo.CostosRepository(ruteo.CostosLoader())
        costos_repository.load('{0}caba_costos.csv'.format(ruteo.DIR_DATOS))

        grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())

        grafo = grafo_io.load('{0}caba_grafo_distancia.edgelist'.format(ruteo.DIR_GRAFOS))
        red = grafo_io.load('{0}caba_red_distancia.edgelist'.format(ruteo.DIR_REDES))

        vertice_converter = ruteo.VerticeConverter()

        return ruteo.RecorridosServer(coordenadas_repository, geometrias_repository, datos_repository,
                                      costos_repository,
                                      grafo, red, vertice_converter)

    @staticmethod
    def get_recorrido_server_pie():
        coordenadas_repository = ruteo.CoordenadasRepository(ruteo.SRID_IN, ruteo.CoordenadasLoader())
        coordenadas_repository.load('{0}caba_coordenadas.csv'.format(ruteo.DIR_GEOMETRIAS))

        geometrias_repository = ruteo.GeometriasRepository(ruteo.SRID_IN, ruteo.GeometriasLoader())
        geometrias_repository.load('{0}caba_geometrias.csv'.format(ruteo.DIR_GEOMETRIAS))

        datos_repository = ruteo.DatosRepository(ruteo.DatosLoader())
        datos_repository.load('{0}caba_datos.csv'.format(ruteo.DIR_DATOS))

        costos_repository = ruteo.CostosRepository(ruteo.CostosLoader())
        costos_repository.load('{0}caba_costos_pie.csv'.format(ruteo.DIR_DATOS))

        grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())

        grafo = grafo_io.load('{0}caba_grafo_pie.edgelist'.format(ruteo.DIR_GRAFOS))
        red = grafo_io.load('{0}caba_red_pie.edgelist'.format(ruteo.DIR_REDES))

        vertice_converter_pass = ruteo.VerticeConverterPass()

        return ruteo.RecorridosServer(coordenadas_repository, geometrias_repository, datos_repository,
                                      costos_repository,
                                      grafo, red, vertice_converter_pass)

    @staticmethod
    def get_recorrido_server_bsas_osm_auto_distancia():
        coordenadas_repository = ruteo.CoordenadasRepository(ruteo.SRID_IN, ruteo.CoordenadasLoader())
        coordenadas_repository.load('{0}bsas_osm_coordenadas.csv'.format(ruteo.DIR_GEOMETRIAS))

        geometrias_repository = ruteo.GeometriasRepository(ruteo.SRID_IN, ruteo.GeometriasLoader())
        geometrias_repository.load('{0}bsas_osm_geometrias.csv'.format(ruteo.DIR_GEOMETRIAS))

        datos_repository = ruteo.DatosRepository(ruteo.DatosLoader())
        datos_repository.load('{0}bsas_osm_datos.csv'.format(ruteo.DIR_DATOS))

        costos_repository = ruteo.CostosRepository(ruteo.CostosLoader())
        costos_repository.load('{0}bsas_osm_costos.csv'.format(ruteo.DIR_DATOS))

        grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())

        grafo = grafo_io.load('{0}bsas_osm_grafo_distancia.edgelist'.format(ruteo.DIR_GRAFOS))
        red = grafo_io.load('{0}bsas_osm_red_distancia.edgelist'.format(ruteo.DIR_REDES))

        vertice_converter = ruteo.VerticeConverter()

        return ruteo.RecorridosServer(coordenadas_repository, geometrias_repository, datos_repository,
                                      costos_repository,
                                      grafo, red, vertice_converter)

    @staticmethod
    def get_recorrido_server_auto_bsas():
        coordenadas_repository = ruteo.CoordenadasRepository(ruteo.SRID_IN, ruteo.CoordenadasLoader())
        coordenadas_repository.load('{0}caba_coordenadas.csv'.format(ruteo.DIR_GEOMETRIAS))

        geometrias_repository = ruteo.GeometriasRepository(ruteo.SRID_IN, ruteo.GeometriasLoader())
        geometrias_repository.load('{0}caba_geometrias.csv'.format(ruteo.DIR_GEOMETRIAS))

        datos_repository = ruteo.DatosRepository(ruteo.DatosLoader())
        datos_repository.load('{0}caba_datos.csv'.format(ruteo.DIR_DATOS))

        costos_repository = ruteo.CostosRepository(ruteo.CostosLoader())
        costos_repository.load('{0}caba_costos.csv'.format(ruteo.DIR_DATOS))

        grafo_io = ruteo.GrafoIO(ruteo.GrafoLoader(), ruteo.GrafoSaver())

        grafo = grafo_io.load('{0}bsas_grafo.edgelist'.format(ruteo.DIR_GRAFOS))
        red = grafo_io.load('{0}bsas_red.edgelist'.format(ruteo.DIR_REDES))

        vertice_converter = ruteo.VerticeConverter()

        return ruteo.RecorridosServer(coordenadas_repository, geometrias_repository, datos_repository,
                                      costos_repository,
                                      grafo, red, vertice_converter)