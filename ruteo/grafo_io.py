# -*- coding: utf-8 -*
class GrafoIO(object):
    def __init__(self, grafo_loader, grafo_saver):
        self._grafo_loader = grafo_loader
        self._grafo_saver = grafo_saver

    def load(self, file_in):
        return self._grafo_loader.load(file_in)

    def save(self, grafo, file_out):
        self._grafo_saver.save(grafo, file_out)