# -*- coding: utf-8 -*-


class SridDecorator(object):
    def __init__(self, geometrias_repository):
        self._geometrias_repository = geometrias_repository
        # self._srid_to = ruteo.SRID_TO
        #self._srid_in = ruteo.SRID_IN

    def decorate(self, srid_to, dic):
        # if not srid_to == self._srid_in:
        # source = osr.SpatialReference()
        #     source.ImportFromEPSG(self._srid_in)
        #
        #     target = osr.SpatialReference()
        #     target.ImportFromEPSG(srid_to)
        #
        #     transform = osr.CoordinateTransformation(source, target)
        #
        #     for key in dic:
        #         dic[key]['geometria'].Transform(transform)

        for key in dic:
            self._geometrias_repository.transform_out(srid_to, dic[key]['geometria'])

        return dic