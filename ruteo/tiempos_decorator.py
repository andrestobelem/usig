# -*- coding: utf-8 -*-
import ruteo


class TiemposDecorator(object):
    def __init__(self):
        #self._costos_repository = costos_repository
        self._TIEMPO_FACTOR = ruteo.TIEMPO_FACTOR

    def decorate(self, dic):
        for key in dic:
            costo = dic[key]['costo']
            dic[key].update({'tiempo': costo / self._TIEMPO_FACTOR, })
        return dic