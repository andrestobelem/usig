# -*- coding: utf-8 -*-
import networkx as nx


class GrafoLoader(object):
    def __init__(self):
        pass

    def load(self, file_in):
        grafo = nx.read_weighted_edgelist(file_in, create_using=nx.DiGraph(), nodetype=str)
        return grafo