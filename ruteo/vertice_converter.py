# -*- coding: utf-8 -*-
from itertools import groupby


class VerticeConverter(object):
    def __init__(self):
        pass

    def contraer_vertice(self, vertice):
        return vertice.split('.')[0].replace('-', '')

    def contraer_vertices(self, recorrido):
        return [vertice for vertice, values in groupby(map(self.contraer_vertice, recorrido))]

    def crear_vertice_in(self, arista_in):
        return '{0}.{1}'.format(self.contraer_vertice(arista_in[1]), self.contraer_vertice(arista_in[0]))

    def crear_vertice_out(self, arista_out):
        return '-{0}.{1}'.format(self.contraer_vertice(arista_out[0]), self.contraer_vertice(arista_out[1]))


class VerticeConverterPass(object):
    def __init__(self):
        pass

    def contraer_vertice(self, vertice):
        return vertice

    def contraer_vertices(self, recorrido):
        return [vertice for vertice, values in groupby(map(self.contraer_vertice, recorrido))]

    def crear_vertice_in(self, arista_in):
        return '{0}'.format(self.contraer_vertice(arista_in[1]))

    def crear_vertice_out(self, arista_out):
        return '{0}'.format(self.contraer_vertice(arista_out[0]))