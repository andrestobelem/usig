# -*- coding: utf-8 -*-
import csv

from osgeo import ogr


class CoordenadasLoader(object):
    def __init__(self):
        pass

    def load(self, srs, file_in):
        coordenadas = csv.reader(open(file_in), delimiter=';', quotechar='"')

        result = dict()

        # srs = osr.SpatialReference()
        # srs.ImportFromEPSG(srid)

        for coordenada in coordenadas:
            ewkt = coordenada[1].split(';')
            wkt = ewkt[1]

            result[coordenada[0]] = ogr.CreateGeometryFromWkt(wkt, srs)

        return result