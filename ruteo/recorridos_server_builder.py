# -*- coding: utf-8 -*-
import ruteo


class RecorridosServerBuilder(object):
    def __init__(self):
        pass

    @staticmethod
    def get_recorrido_server():
        #coordenadas_repository = ruteo.CoordenadasRepository(ruteo.SRID_IN, ruteo.CoordenadasLoader())
        #coordenadas_repository.load('{0}coordenadas.csv'.format(ruteo.DIR_GEOMETRIAS))

        geometrias_repository = ruteo.GeometriasRepository(ruteo.SRID_IN, ruteo.GeometriasLoader())
        geometrias_repository.load('{0}geometrias.csv'.format(ruteo.DIR_GEOMETRIAS))

        datos_repository = ruteo.DatosRepository(ruteo.DatosLoader())
        datos_repository.load('{0}datos.csv'.format(ruteo.DIR_DATOS))

        #return ruteo.RecorridosServer(coordenadas_repository, geometrias_repository, datos_repository)
        return ruteo.RecorridosServer(geometrias_repository, datos_repository)