# -*- coding: utf-8 -*-
from osgeo import osr, ogr


class GeometriasRepository(object):
    def __init__(self, srid, geometrias_loader):
        self._geometrias_loader = geometrias_loader
        self._geometrias = None
        self._srid = srid

        self._srs = osr.SpatialReference()
        self._srs.ImportFromEPSG(self._srid)

    def load(self, file_in):
        self._geometrias = self._geometrias_loader.load(self._srs, file_in)

    def get_srid(self):
        return self._srid

    def get_srs(self):
        return self._srs

    def transform_in(self, srid_from, geometria):
        if not srid_from == self._srid:
            source = osr.SpatialReference()
            source.ImportFromEPSG(srid_from)

            target = self.get_srs()

            transform = osr.CoordinateTransformation(source, target)

            geometria.Transform(transform)
        return geometria

    def transform_out(self, srid_to, geometria):
        if not srid_to == self._srid:
            source = self.get_srs()

            target = osr.SpatialReference()
            target.ImportFromEPSG(srid_to)

            transform = osr.CoordinateTransformation(source, target)

            geometria.Transform(transform)
        return geometria

    def get(self, key):
        return self._geometrias[key]

    def get_geometria(self, key):
        return self._geometrias[key]['geometria'].Clone()

    def get_longitud(self, key):
        return self._geometrias[key]['longitud']

    # def add_geometria(self, key, geometria):
    #     self._geometrias.update({key: geometria})
    #
    # def del_geometria(self, key):
    #     del self._geometrias[key]

    def get_tramo_mas_cercano(self, punto):
        d = {k: punto.Distance(v['geometria']) for k, v in self._geometrias.iteritems()}
        return min(d, key=d.get)

    def get_segmento_mas_cercano(self, geometria, punto):
        min_dist = None

        it = iter(geometria.GetPoints())
        start = it.next()

        while True:
            try:
                end = it.next()
            except StopIteration:
                break

            line = ogr.Geometry(ogr.wkbLineString)
            line.AddPoint_2D(start[0], start[1])
            line.AddPoint_2D(end[0], end[1])

            dist = line.Distance(punto)

            if min_dist is None or dist < min_dist:
                min_dist = dist
                seg = (start, end)

            start = end

        return seg

    def get_punto_mas_cercano(self, segmento, punto):
        a, b = segmento
        ax, ay = a
        bx, by = b
        px = punto.GetX()
        py = punto.GetY()

        if a == b:
            return a

        r = ((px - ax) * (bx - ax) + (py - ay) * (by - ay)) / \
            ((bx - ax) * (bx - ax) + (by - ay) * (by - ay))

        if r < 0:
            return a

        elif r > 1:
            return b

        return ax + ((bx - ax) * r), ay + ((by - ay) * r)

    def invertir_linea(self, linea):
        result = linea.Clone()
        result.Empty()

        for i in range(linea.GetPointCount()-1, -1, -1):
            (x, y) = linea.GetPoint_2D(i)
            result.AddPoint_2D(x, y)

        return result

    def concatenar_lineas(self, linea1, linea2):

        result = linea1.Clone()

        srs1 = linea1.GetSpatialReference()
        srs2 = linea2.GetSpatialReference()

        if linea1.GetPoint_2D(linea1.GetPointCount() - 1) == linea2.GetPoint_2D(0) and srs1.IsSame(srs2):
            for i in range(1, linea2.GetPointCount()):
                (x, y) = linea2.GetPoint_2D(i)
                result.AddPoint_2D(x, y)

        elif linea1.GetPoint_2D(0) == linea2.GetPoint_2D(linea2.GetPointCount() - 1) and srs1.IsSame(srs2):
            result = self.invertir_linea(result)
            for i in range(linea2.GetPointCount()-1, -1, -1):
                (x, y) = linea2.GetPoint_2D(i)
                result.AddPoint_2D(x, y)

        elif linea1.GetPoint_2D(linea1.GetPointCount() - 1) == linea2.GetPoint_2D(linea2.GetPointCount() - 1) and srs1.IsSame(srs2):
            for i in range(linea2.GetPointCount()-1, -1, -1):
                (x, y) = linea2.GetPoint_2D(i)
                result.AddPoint_2D(x, y)

        elif linea1.GetPoint_2D(0) == linea2.GetPoint_2D(0) and srs1.IsSame(srs2):
            result = self.invertir_linea(result)
            for i in range(1, linea2.GetPointCount()):
                (x, y) = linea2.GetPoint_2D(i)
                result.AddPoint_2D(x, y)
        else:

            print linea1
            print linea2
            raise Exception("Error en las geometrías")

        return result

    def cortar_geometria_origen(self, linea, punto):
        segmento_mas_cercano = self.get_segmento_mas_cercano(linea, punto)
        punto_mas_cercano = self.get_punto_mas_cercano(segmento_mas_cercano, punto)

        srs = linea.GetSpatialReference()
        line = ogr.Geometry(ogr.wkbLineString)
        line.AssignSpatialReference(srs)

        no_saltear = False
        for p in linea.GetPoints():
            if no_saltear:
                line.AddPoint_2D(p[0], p[1])

            if p == segmento_mas_cercano[0]:
                line.AddPoint_2D(punto_mas_cercano[0], punto_mas_cercano[1])
                no_saltear = True

        return line

    def cortar_geometria_destino(self, linea, punto):
        segmento_mas_cercano = self.get_segmento_mas_cercano(linea, punto)
        punto_mas_cercano = self.get_punto_mas_cercano(segmento_mas_cercano, punto)

        srs = linea.GetSpatialReference()
        line = ogr.Geometry(ogr.wkbLineString)
        line.AssignSpatialReference(srs)

        for p in linea.GetPoints():
            line.AddPoint_2D(p[0], p[1])

            if p == segmento_mas_cercano[0]:
                break

        line.AddPoint_2D(punto_mas_cercano[0], punto_mas_cercano[1])

        return line

    def cortar_geometria(self, linea, punto_origen, punto_destino):
        origen_segmento_mas_cercano = self.get_segmento_mas_cercano(linea, punto_origen)
        origen_punto_mas_cercano = self.get_punto_mas_cercano(origen_segmento_mas_cercano, punto_origen)

        destino_segmento_mas_cercano = self.get_segmento_mas_cercano(linea, punto_destino)
        destino_punto_mas_cercano = self.get_punto_mas_cercano(destino_segmento_mas_cercano, punto_destino)

        srs = linea.GetSpatialReference()
        line = ogr.Geometry(ogr.wkbLineString)
        line.AssignSpatialReference(srs)

        no_saltear = False
        for p in linea.GetPoints():
            if no_saltear:
                line.AddPoint_2D(p[0], p[1])

            if p == origen_segmento_mas_cercano[0]:
                line.AddPoint_2D(origen_punto_mas_cercano[0], origen_punto_mas_cercano[1])
                no_saltear = True

            if p == destino_segmento_mas_cercano[0]:
                break

        line.AddPoint_2D(destino_punto_mas_cercano[0], destino_punto_mas_cercano[1])

        return line


