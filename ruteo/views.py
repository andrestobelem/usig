# -*- coding: utf-8 -*-
import urllib2
import urllib

from flask import Response, json, request

import ruteo
from ruteo import app


@app.route('/')
def index():
    return 'Servidor de ruteo'


@app.route('/buscar_recorrido/')
def buscar_recorrido():
    red = request.args.get('red', None)
    if red is not None:
        if red == 'auto':
            red = 'vial'
        if red == 'pie':
            red = 'peatonal'
        if red == 'bicicleta' or red == 'bici':
            red = 'bicicleta'
            pass
    else:
        red = 'peatonal'

    origen = request.args.get('origen', None)
    if origen is not None:
        origen = origen.split(',')
        origen_x = float(origen[0])
        origen_y = float(origen[1])
    else:
        origen_x = None
        origen_y = None

    destino = request.args.get('destino', None)
    if destino is not None:
        destino = destino.split(',')
        destino_x = float(destino[0])
        destino_y = float(destino[1])
    else:
        destino_x = None
        destino_y = None

    origen_calle = request.args.get('origen_calle', None)
    if origen_calle is not None:
        try:
            origen_calle = int(origen_calle)
        except ValueError:
            origen_calle = str(origen_calle)

    origen_altura = request.args.get('origen_altura', None)
    if origen_altura is not None:
        origen_altura = int(origen_altura)

    origen_cruce = request.args.get('origen_cruce', None)
    if origen_cruce is not None:
        try:
            origen_cruce = int(origen_cruce)
        except ValueError:
            origen_cruce = str(origen_cruce)

    destino_calle = request.args.get('destino_calle', None)
    if destino_calle is not None:
        try:
            destino_calle = int(destino_calle)
        except ValueError:
            destino_calle = str(destino_calle)

    destino_altura = request.args.get('destino_altura', None)
    if destino_altura is not None:
        destino_altura = int(destino_altura)

    destino_cruce = request.args.get('destino_cruce', None)
    if destino_cruce is not None:
        try:
            destino_cruce = int(destino_cruce)
        except ValueError:
            destino_cruce = str(destino_cruce)

    distancia = bool(int(request.args.get('distancia', 0)))
    autopistas = bool(int(request.args.get('autopistas', 0)))
    cortes = bool(int(request.args.get('cortes', 0)))

    srid_from = int(request.args.get('srid_from', ruteo.SRID_FROM))
    srid_to = int(request.args.get('srid_to', ruteo.SRID_TO))

    origen_geocoder = False
    destino_geocoder = False

    if origen is None:
        if not origen_altura is None:
            params = {"cod_calle": origen_calle, "altura": origen_altura}
            data = json.loads(
                urllib2.urlopen(ruteo.BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            origen_x = float(data['x'])
            origen_y = float(data['y'])
            origen_geocoder = True

        elif not origen_cruce is None:
            params = {"cod_calle1": origen_calle, "cod_calle2": origen_cruce}
            data = json.loads(
                urllib2.urlopen(ruteo.BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            origen_x = float(data['x'])
            origen_y = float(data['y'])
            origen_geocoder = True

    elif origen_calle is None:
        params = {"x": origen_x, "y": origen_y}
        data = json.loads(
            urllib2.urlopen(ruteo.BASE_PATH_REVERSE_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])

        try:
            origen_data = data['puerta'].split(' ')
            origen_altura = int(origen_data.pop())
            origen_calle = str(' '.join(origen_data))
            origen_x = float(data['puerta_x'])
            origen_y = float(data['puerta_y'])
            origen_geocoder = True
        except AttributeError:
            pass

    if destino is None:
        if not destino_altura is None:
            params = {"cod_calle": destino_calle, "altura": destino_altura}
            data = json.loads(
                urllib2.urlopen(ruteo.BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            destino_x = float(data['x'])
            destino_y = float(data['y'])
            destino_geocoder = True

        elif not destino_cruce is None:
            params = {"cod_calle1": destino_calle, "cod_calle2": destino_cruce}
            data = json.loads(
                urllib2.urlopen(ruteo.BASE_PATH_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
            destino_x = float(data['x'])
            destino_y = float(data['y'])
            destino_geocoder = True

    elif destino_calle is None:
        params = {"x": destino_x, "y": destino_y}
        data = json.loads(
            urllib2.urlopen(ruteo.BASE_PATH_REVERSE_GEOCODING + "?" + urllib.urlencode(params), " ").read()[1:-1])
        try:
            destino_data = data['puerta'].split(' ')
            destino_altura = int(destino_data.pop())
            destino_calle = str(' '.join(destino_data))
            destino_x = float(data['puerta_x'])
            destino_y = float(data['puerta_y'])
            destino_geocoder = True
        except AttributeError:
            pass

    parameters = dict(
        origen_calle=origen_calle,
        origen_altura=origen_altura,
        origen_cruce=origen_cruce,
        destino_calle=destino_calle,
        destino_altura=destino_altura,
        destino_cruce=destino_cruce,
        origen_x=origen_x,
        origen_y=origen_y,
        destino_x=destino_x,
        destino_y=destino_y,
        srid_from=srid_from,
        srid_to=srid_to,
        red=red,
        distancia=distancia,
        autopistas=autopistas,
        cortes=cortes,
        origen_geocoder=origen_geocoder,
        destino_geocoder=destino_geocoder,
    )

    recorrido = ruteo.recorridos_server.get_recorrido(**parameters)

    return Response(json.dumps(recorrido), mimetype='application/json')