# -*- coding: utf-8 -*-
class GrafoExpander(object):
    def __init__(self, vertice_expander):
        self._vertice_expander = vertice_expander

    def expandir_grafo(self, grafo):
        vertices = grafo.nodes()

        for vertice in vertices:
            self._vertice_expander.expandir_vertice(grafo, vertice)
        return grafo